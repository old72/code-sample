# WITSML Source App

This is the WITSML Source app which polls a WITSML server to provide data to the app stream. It is invoked via the scheduler every 1 minute.

### Building

```
#!console

dotnet restore
dotnet build
```


### Package for deploy 

```
#!console

dotnet publish
```
- Zip Contents (not publish folder itself)
See more: http://docs.aws.amazon.com/lambda/latest/dg/lambda-dotnet-coreclr-deployment-package.html

In project folder use

```
#!console

dotnet lambda package
```
You get zip package for deploying.
You can also use "dotnet lambda deploy-function" for auto deploy.
See more: http://docs.aws.amazon.com/toolkit-for-visual-studio/latest/user-guide/lambda-cli-publish.html


### Expect Config Passed to the lambda
Expected lambda input:

```
#!json

{
    "version": "1",
    "collection": "wits.raw",
    "server_url": "XXXXX",
    "server_user": "XXX",
    "server_password": "XXX",
    "environment": "qa",
    "app": 9,
    "app_key": "corva.source",
    "app_connection": 1,
    "company": 1,
    "provider": "corva",
    "api_url": "XXXX",
    "api_key": "XXXXX",
    "asset_id": 14,
    "asset_name": "Well 2",
    "asset_type": "Well",
    "rig_name": "",
    "well_name": "XXXXX",
    "wellbore_log": "XXXXXX"
}
```

For historical import from S3:

```
#!json

{
  ...
  "bucket": "import-server-test",
  "key_name": "1486899600.csv",
  ...
}
```

### Lammbda Function Handler

```
#!console

Witsml.SourceApp::Witsml.SourceApp.LambdaHandler::Function
```
