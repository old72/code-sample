﻿using System.Threading.Tasks;
using Witsml.SourceApp.ApiRequests;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.Authorization
{
    public class Authorizer
    {
        private readonly IAppSettingProvider _appSettingProvider;
        private AuthResponse _authResponse;

        public Authorizer(IAppSettingProvider appSettingProvider)
        {
            _appSettingProvider = appSettingProvider;
            string apiKey = _appSettingProvider.ApiKey;

            if (!string.IsNullOrEmpty(appSettingProvider.ApiKey))
            {
                _authResponse = new AuthResponse { AuthKey = apiKey };
            }
        }

        public async Task<AuthResponse> GetAuthKey()
        {
            if (_authResponse != null)
            {
                return _authResponse;
            }

            var authRequestBody = PrepareAuthRequestBody();

            var postRequest = new AuthTokenRequest(authRequestBody);

            var response = await postRequest.ExecuteAsync();

            if (response.HttpResponseMessage.IsSuccessStatusCode)
            {
                _authResponse = response.Response;
            }
                
            return _authResponse;
        }

        private AuthRequestBody PrepareAuthRequestBody()
        {
            return new AuthRequestBody
            {
                Auth = new Auth
                {
                    Password = _appSettingProvider.CorvaUserPassword,
                    Email = _appSettingProvider.CorvaUserEmail
                }
            };
        }
    }
}
