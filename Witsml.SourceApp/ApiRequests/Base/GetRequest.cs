﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Witsml.SourceApp.ApiRequests.Base
{
    public class GetRequest<TResponse> : AuthBaseRequest<TResponse>
    {
        public GetRequest(string requestUri) : base(requestUri)
        {
        }

        protected override async Task<HttpResponseMessage> GetHttpResponseMessageAsync(HttpClient client, HttpContent content)
        {
            return await client.GetAsync(RequestUri);
        }
    }
}
