﻿using Newtonsoft.Json;

namespace Witsml.SourceApp.ApiRequests.Base
{
    public abstract class SerializedBodyRequest<TRequest, TResponse> : AuthBaseRequest<TResponse>
    {
        private readonly TRequest _requestContent;

        protected SerializedBodyRequest(string requestUri, TRequest requestContent) : base(requestUri)
        {
            _requestContent = requestContent;
        }

        protected override void SetBody()
        {
            var serializedRequestContent = JsonConvert.SerializeObject(_requestContent);
            BodyContent = serializedRequestContent;
        }
    }
}
