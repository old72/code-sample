﻿using Autofac;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Witsml.SourceApp.Authorization;
using Witsml.SourceApp.IoC;

namespace Witsml.SourceApp.ApiRequests.Base
{
    public abstract class AuthBaseRequest<TResponse> : BaseRequest<TResponse>
    {
        protected AuthBaseRequest(string requestUri)
        {
            RequestUriString = requestUri;
        }

        protected override async Task<AuthenticationHeaderValue> GetAuthenticationHeaderValue()
        {
            var authorizer = Bootstrapper.GetContainer().Resolve<Authorizer>();
            var authResponse = await authorizer.GetAuthKey();
            return new AuthenticationHeaderValue("API", authResponse.AuthKey);
        }
    }
}