﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Shared.Logging;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.IoC;
using Witsml.SourceApp.Model.ApiModel;
using Autofac;

namespace Witsml.SourceApp.ApiRequests.Base
{
    public abstract class BaseRequest<TResponse>
    {
        private readonly IAppSettingProvider _appSettingProvider;

        private ILogger _logger { get; }
        public string BodyContent { get; protected set; }
        public string RequestUriString { get; protected set; }
        public Uri RequestUri { get; private set; }

        protected BaseRequest()
        {
            _appSettingProvider = Bootstrapper.GetContainer().Resolve<IAppSettingProvider>();
            _logger = Bootstrapper.GetContainer().Resolve<ILogger>();
        }

        public async Task<ApiResponse<TResponse>> ExecuteAsync()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettingProvider.CorvaBaseAddress);

                    // requesting authorization token
                    var authorization = await GetAuthenticationHeaderValue();
                    if (authorization != null)
                    {
                        client.DefaultRequestHeaders.Authorization = authorization;
                    }

                    SetBody();

                    // preparing http content for possible request body string
                    var content = GetHttpContent();

                    SetHeaders(content);

                    RequestUri = PrepareUri();

                    // requesting resource 
                    var response = await GetHttpResponseMessageAsync(client, content);
                    // reading response
                    var responseAsString = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        var deserializedResponse = JsonConvert.DeserializeObject<TResponse>(responseAsString);
                        return ApiResponse<TResponse>.ConstructApiResponse(response, deserializedResponse);
                    }

                    //var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseAsString);
                    //log not successful response
                    string errorMessage = $"Response from {response.RequestMessage.RequestUri} - [Error] : '{response.ReasonPhrase}', [Status]: {response.StatusCode}, [Msg]: '{response.RequestMessage}'";
                    _logger.LogError(errorMessage);

                    return ApiResponse<TResponse>.ConstructApiResponse(response, default(TResponse));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                throw;
            }
        }

        protected virtual HttpContent GetHttpContent()
        {
            if (!string.IsNullOrEmpty(BodyContent))
            {
                return new StringContent(BodyContent, Encoding.UTF8, "application/json");
            }

            return null;
        }

        protected virtual void SetBody() { }

        protected virtual async Task<AuthenticationHeaderValue> GetAuthenticationHeaderValue()
        {
            return await Task.FromResult(default(AuthenticationHeaderValue));
        }

        protected virtual Uri PrepareUri()
        {
            return new Uri(RequestUriString, UriKind.Relative);
        }

        protected virtual void SetHeaders(HttpContent content) { }

        protected abstract Task<HttpResponseMessage> GetHttpResponseMessageAsync(HttpClient client, HttpContent content);
    }
}