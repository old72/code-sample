﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Witsml.SourceApp.ApiRequests.Base
{
    public class PostRequest<TRequest, TResponse> : SerializedBodyRequest<TRequest, TResponse>
    {
        public PostRequest(string requestUri, TRequest requestContent) : base(requestUri, requestContent)
        {
        }

        protected override async Task<HttpResponseMessage> GetHttpResponseMessageAsync(HttpClient client, HttpContent content)
        {
            return await client.PostAsync(RequestUri, content);
        }
    }
}