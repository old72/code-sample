﻿using Witsml.SourceApp.ApiRequests.Base;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.ApiRequests
{
    public class CreateNewAssetRequest : PostRequest<Asset, Asset>
    {
        public CreateNewAssetRequest(Asset requestContent) : base("/v1/assets", requestContent)
        {
        }
    }
}