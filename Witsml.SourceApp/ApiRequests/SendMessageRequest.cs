﻿using System;
using System.Collections.Generic;
using Witsml.SourceApp.ApiRequests.Base;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.ApiRequests
{
    public class SendMessageRequest<T> : PostRequest<SendMessageRequestBody<T>, object>
    {
        private readonly string _appKey;

        public SendMessageRequest(string appKey, SendMessageRequestBody<T> requestContent) : base($"/v1/message_producer/{appKey}", requestContent)
        {
            _appKey = appKey;
        }
    }
}