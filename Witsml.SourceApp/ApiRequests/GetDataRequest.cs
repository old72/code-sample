﻿using System;
using System.Collections.Generic;
using System.Text;
using Witsml.SourceApp.ApiRequests.Base;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.ApiRequests
{
    public class GetDataRequest : GetRequest<Dictionary<string, object>>
    {
        private readonly string _key;
        
        public GetDataRequest(string key) : base("/v1/cache/")
        {
            _key = key;
        }

        protected override Uri PrepareUri()
        {
            var str = RequestUriString + _key;
            return new Uri(str, UriKind.Relative);
        }
    }
}
