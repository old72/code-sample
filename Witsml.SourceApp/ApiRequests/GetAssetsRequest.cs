﻿using System;
using System.Collections.Generic;
using System.Text;
using Witsml.SourceApp.ApiRequests.Base;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.ApiRequests
{
    public class GetAssetsRequest : GetRequest<List<Asset>>
    {
        private readonly string _types;
        private readonly string _search;

        /// <summary>
        /// Ctor for get assets request
        /// </summary>
        /// <param name="types">types of results to return. Default: all. Possible values: all, rig, well, program</param>
        /// <param name="search">search term for asset name</param>
        public GetAssetsRequest(string types, string search) : base("/v1/assets")
        {
            _types = types;
            _search = search;
        }

        protected override Uri PrepareUri()
        {
            var str = RequestUriString + BuildQueryString();
            var escaped = str;
            return new Uri(escaped, UriKind.Relative);
        }

        private string BuildQueryString()
        {
            var sb = new StringBuilder();
            sb.Append("?types=");
            sb.Append(Uri.EscapeDataString(_types));
            sb.Append("&search=");
            sb.Append(Uri.EscapeDataString(_search));
            return sb.ToString();
        }
    }
}
