﻿using System;
using System.Collections.Generic;
using Witsml.SourceApp.ApiRequests.Base;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.ApiRequests
{
    public class SaveDataRequest : PostRequest<Dictionary<string, object>, string>
    {
        private readonly string _key;

        public SaveDataRequest(string key, Dictionary<string, object> requestContent) : base("/v1/cache/", requestContent)
        {
            _key = key;
        }

        protected override Uri PrepareUri()
        {
            var str = RequestUriString + _key;
            return new Uri(str, UriKind.Relative);
        }
    }
}