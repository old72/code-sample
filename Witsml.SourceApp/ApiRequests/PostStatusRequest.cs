﻿using System;
using System.Collections.Generic;
using Witsml.SourceApp.ApiRequests.Base;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.ApiRequests
{
    public class PostStatusRequest : PostRequest<object, PostStatusResponse>
    {
        private readonly string _scheduleId;
        private readonly string _status;

        public PostStatusRequest(string scheduleId, string status, object requestContent = null) : base("/scheduler/", requestContent)
        {
            _scheduleId = scheduleId;
            _status = status;
        }

        protected override Uri PrepareUri()
        {
            var str = $"{RequestUriString}{_scheduleId}/{_status}"; // /schedule/{schedule_id}/{status}. status should be processing or completed.
            return new Uri(str, UriKind.Relative);
        }
    }
}