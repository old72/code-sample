﻿using System.Net.Http.Headers;
using System.Threading.Tasks;
using Witsml.SourceApp.ApiRequests.Base;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.ApiRequests
{
    public class AuthTokenRequest : PostRequest<AuthRequestBody, AuthResponse>
    {
        public AuthTokenRequest(AuthRequestBody requestContent) : base("/v1/user_token", requestContent)
        {
        }

        protected override async Task<AuthenticationHeaderValue> GetAuthenticationHeaderValue()
        {
            return await Task.FromResult(default(AuthenticationHeaderValue));
        }
    }
}