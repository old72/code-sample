﻿using Witsml.SourceApp.ApiRequests.Base;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.ApiRequests
{
    public class UpdateAssetRequest : PostRequest<AssetRequestBody, Asset>
    {
        public UpdateAssetRequest(long assetId, AssetRequestBody requestContent) : base($"/v1/assets/{assetId}", requestContent)
        {
        }
    }
}
