﻿using System.Threading;

namespace Witsml.SourceApp.Interfaces
{
    public interface ICancellationTokenSourceProvider
    {
        CancellationTokenSource Create();
    }
}