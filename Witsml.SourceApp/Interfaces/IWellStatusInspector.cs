﻿using System.Collections.Generic;
using nwitsml.Core;

namespace Witsml.SourceApp.Interfaces
{
    public interface IWellStatusInspector
    {
        void Init(WitsmlServer server);

        string ActiveWellId { get; }
        string ActiveWellName { get; }
        string ActiveWellboreId { get; }

        void Inspect();

        void StopInspector();
    }
}
