using System;
using System.Collections.Generic;
using Witsml.SourceApp.Model;

namespace Witsml.SourceApp.Interfaces
{
    public interface IDataReader : IDisposable
    {
        bool Init();
        string ReadLine();
    }
}