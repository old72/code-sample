﻿using System;
using System.Collections.Generic;
using Witsml.SourceApp.Model;

namespace Witsml.SourceApp.Interfaces
{
    public interface IDataRetriever
    {
        event EventHandler<EventArgs> DataRetrieverReady;
        event EventHandler<EventArgs> DataRetrieveComplete;

        bool IsCompleted { get; }

        void Connect();

        bool GetWellData(DateTime startDateTimeIndex, System.Threading.CancellationToken _getWitsmlToken);

        List<WitsmlDtoBase> GetWitsmlData(int startIndex, int rowsToRead);

        int GetRowCount();

        DateTime GetDateTimeByIndex(int endIndex);

        void Stop();

        DateTime GetNextStartIndex(DateTime lastIndex);

        int GetWaitSeconds();
    }
}
