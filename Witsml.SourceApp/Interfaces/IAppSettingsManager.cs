﻿using System.Collections.Generic;

namespace Witsml.SourceApp.Interfaces
{
    public interface IAppSettingsManager
    {
        T GetValue<T>(string key);
        void UpdateSettings(Dictionary<string, object> settings);
    }
}
