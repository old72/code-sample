﻿using System;

namespace Witsml.SourceApp.Interfaces
{
    public interface ILogger
    {
        void LogInfo(string text);
        void LogWarning(string text);
        void LogError(string text);
        void LogError(Exception ex);
        void SetPrefix(string v);
    }
}