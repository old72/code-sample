﻿using System;

namespace Witsml.SourceApp.Interfaces
{
    public interface IFileDownloader
    {
        string FilePath { get; set; }
        event EventHandler<EventArgs> DownloadComplete;
        void DownloadFromUrl(string url);
    }
}
