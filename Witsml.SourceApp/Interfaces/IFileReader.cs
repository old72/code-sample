using System;
using System.Collections.Generic;
using Witsml.SourceApp.Model;

namespace Witsml.SourceApp.Interfaces
{
    public interface IFileReader
    {
        bool Init(string filePath);
        DateTime GetLineDateTime(string line = null);
        void Dispose();
        bool ReadNextDataBatch(DateTime startDateTimeIndex);
        List<WitsmlDtoBase> GetWitsmlData(int startIndex, int rowsToRead);
        WitsmlDtoBase ToWits(string inputLine);
        DateTime GetDateTimeByIndex(int index);
        int GetRowCount();
        DateTime GetNextDateTime(DateTime lastIndex);
    }
}