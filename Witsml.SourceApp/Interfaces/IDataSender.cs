﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Witsml.SourceApp.Model;

namespace Witsml.SourceApp.Interfaces
{
    public interface IDataSender<T> : IDisposable
    {
        event EventHandler<EventArgs> SenderReady;
        event EventHandler<EventArgs> SenderDisconnected;
        void Init();
        Task<bool> Send(IEnumerable<T> data, RunnerStatus sourceToken);
    }
}
