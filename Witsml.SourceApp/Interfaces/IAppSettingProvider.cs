﻿using System;
using System.Collections.Generic;
using Witsml.SourceApp.Settings;

namespace Witsml.SourceApp.Interfaces
{
    public interface IAppSettingProvider
    {
        DateTime LastImportUtcDateTime { get; set; }
        DateTime LastReadUtcDateTime { get; set; }
        DateTime StartFromUtc { get; }

        int BatchSize { get; }
        string ServerUrl { get; }
        string ServerUser { get; }
        string ServerPassword { get; }
        string LogId { get; }
        string WellId { get; set; }
        string WellboreId { get; set; }
        int AssetId { get; }
        string App { get; }
        string AppId { get; }
        int CompanyId { get; }
        int Version { get; }
        bool ExportDataToFile { get; }
        int ExportedLineCount { get; }
        string KinesisStreamName { get; }
        string SecretKey { get; }
        string AccessKey { get; }
        string RegionName { get; }
        string RigName { get; }
        string OldRigName { get; set; }
        int MaxKinesisRecords { get; }
        string CorvaBaseAddress { get; }
        string CorvaUserPassword { get; }
        string CorvaUserEmail { get; }
        string Provider { get; }
        string Collection { get; }
        string FileNameToExport { get; }
        int interval { get; }
        string schedule { get; }
        string explicit_hash_key { get; }
        int partition_number { get; }

        void UpdateSettings(Dictionary<string, object> settings);
        StateManager SM { set; }
        string ApiKey { get; }
        string Bucket { get; }
        string KeyName { get; }
        string WellName { get; }
        string CsvTimeZone { get; }
        string Log10secId { get; }
        DateTime HardStartFromUtc { get; }
        bool ignore_10s { get; }
        bool CheckLogNames { get; }
        List<string> debug { get; }
        bool debugXml { get; }
        bool debugOutput { get; }
        bool debugRequest { get; }
        bool debugTiming { get; }
        bool debug_file { get; }

        bool ValidateSettings();
    }
}