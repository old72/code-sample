﻿using System.Threading;

namespace Witsml.SourceApp.Interfaces
{
    public interface IReplicator
    {
        void Start(AutoResetEvent autoEvent);
        void Stop(bool immediateStop = false);
    }
}