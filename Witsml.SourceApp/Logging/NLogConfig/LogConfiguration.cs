﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using NLog;
using NLog.Config;
using NLog.Targets;
using static System.String;
using Witsml.SourceApp.Logging;

namespace Shared.Logging.NLogConfig
{
    /// <summary>
    /// Configuration class allowing to add NLog primary targets quickly
    /// 
    /// NLog provides the following log types (in the order of ascending priority)
    /// <ul>
    /// <li> Trace</li>
    /// <li> Debug</li>
    /// <li> Info</li>
    /// <li> Warn</li>
    /// <li> Error</li>
    /// <li> Fatal</li>
    /// </ul>
    /// 
    /// </summary>
    public class LogConfiguration
    {
        private static readonly string logName = "source-app";  // AppDomain.CurrentDomain.FriendlyName;
        private readonly LoggingConfiguration _config = new LoggingConfiguration();
        private static readonly string _layoutFormat = "${date} | ${level:uppercase=true} | a${event-context:item=AssetId}: ${message} ${exception:format=tostring}";

        /// <summary>
        /// Adds the file target. Creates FIFO stack of log archive files.
        /// Logs everything starting from the Trace level.
        /// 
        /// Designed to keep 7 archive files at most (one file a day).
        /// Rewrites the oldest one when the max number of files is reached.
        /// </summary>
        /// <param name="logFileName">Name of the log file.</param>
        public void AddFileTargetWithDailyArchiving(string logFileName)
        {
            string extension = Empty;
            if (Path.HasExtension(logFileName))
            {
                extension = Path.GetExtension(logFileName);
                logFileName = Regex.Replace(logFileName, Format("{0}$", extension), "");
            }
            string newExtension = IsNullOrEmpty(extension) ? ".log" : extension;
            string archiveFileName = Format(
                IsNullOrEmpty(logFileName) ? "{{##}}{1}" : "{0}.{{##}}{1}",
                logFileName, newExtension);
            string newLogFileName = logFileName + newExtension;

            const string fileTargetName = "fileTarget";
            LogLevel minLevel = LogLevel.Trace;

            var fileTarget = new FileTarget
            {
                FileName = archiveFileName,
                Layout = _layoutFormat,
                Name = fileTargetName,
                ArchiveEvery = FileArchivePeriod.Day,
                ArchiveFileName = archiveFileName
            };
            fileTarget.FileName = newLogFileName;
            fileTarget.ArchiveNumbering = ArchiveNumberingMode.Rolling;
            fileTarget.MaxArchiveFiles = 7;

            var rule = new LoggingRule(logName, minLevel, fileTarget);
            _config.LoggingRules.Add(rule);
            _config.AddTarget(fileTargetName, fileTarget);
            LogManager.Configuration = _config;
        }

        public void AddConsoleTarget()
        {
            LogLevel minLevel = LogLevel.Trace;

            var consoleTarget = new ConsoleTarget
            {
                Layout = _layoutFormat,
                Name = "ConsoleLog"
            };

            var rule = new LoggingRule("*", minLevel, consoleTarget);

            _config.AddTarget(consoleTarget.Name, consoleTarget);
            _config.LoggingRules.Add(rule);
            LogManager.Configuration = _config; // trigger initialization for this target
        }

        /// <summary>
        /// Gets the logger object.
        /// </summary>
        public static Logger LoggerObject()
        {
            return LogManager.GetLogger(logName);
        }
    }
}
