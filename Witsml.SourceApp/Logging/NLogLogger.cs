﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using NLog;
using Shared.Logging.NLogConfig;

namespace Witsml.SourceApp.Logging
{
    public  class NLogLogger: Witsml.SourceApp.Interfaces.ILogger
    {
        private readonly NLog.ILogger _nlogger = LogConfiguration.LoggerObject();
        private string _prefix;

        public NLogLogger()
        {
            var logConfig = new LogConfiguration();
            logConfig.AddConsoleTarget();

            try    // local run with log to file
            {
                var location = Assembly.GetEntryAssembly().Location;
                string logFileName = Path.ChangeExtension(location, "log");
                logConfig.AddFileTargetWithDailyArchiving(logFileName);
            }
            catch { }
        }

        public void LogInfo(string text)
        {
            LogEventInfo theEvent = new LogEventInfo(NLog.LogLevel.Info, "", text);
            theEvent.Properties["AssetId"] = _prefix;
            _nlogger.Log(theEvent);
        }

        public void LogWarning(string text)
        {
            LogEventInfo theEvent = new LogEventInfo(NLog.LogLevel.Warn, "", text);
            theEvent.Properties["AssetId"] = _prefix;
            _nlogger.Log(theEvent);
        }

        public void LogError(string text)
        {
            LogEventInfo theEvent = new LogEventInfo(NLog.LogLevel.Error, "", text);
            theEvent.Properties["AssetId"] = _prefix;
            _nlogger.Log(theEvent);
        }

        public void LogError(Exception ex)
        {
            LogEventInfo theEvent = LogEventInfo.Create(NLog.LogLevel.Error, null, ex, CultureInfo.CurrentCulture, null);
            theEvent.Properties["AssetId"] = _prefix;
            _nlogger.Log(theEvent);
        }

        public void SetPrefix(string prefix)
        {
            this._prefix = prefix;
        }
    }
}
