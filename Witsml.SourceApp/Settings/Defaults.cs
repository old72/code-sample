﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Witsml.SourceApp.Settings
{
    public class Defaults
    {
        public static T getDefault<T>(string key)
        {
            if (AppSettingProvider.DefaultsMap.TryGetValue(key, out object result))
            {
                return (T)Convert.ChangeType(result, typeof(T));
            }

            return default(T);
        }
    }
}
