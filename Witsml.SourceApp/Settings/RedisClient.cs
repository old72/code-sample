﻿using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Witsml.SourceApp.Interfaces;

namespace Witsml.SourceApp.Settings
{
    public class RedisClient : IDisposable
    {
        private readonly string _defaultURL = "redis://localhost";

        public static string _dbURL { get; set; }
        private readonly IAppSettingProvider _appSettingProvider;

        //private ConnectionMultiplexer _redis;
        //private IDatabase _db;
        private bool _isConnected;

        private RedisManagerPool _pool;
        private IRedisClient _client;

        private string _stateKey => $"{_appSettingProvider.Provider}/{_appSettingProvider.App}/asset{_appSettingProvider.AssetId}.settings";

        public RedisClient(IAppSettingProvider appSettingProvider )
        {
            _appSettingProvider = appSettingProvider;
            _isConnected = false;
        }

        //public void Connect()
        //{
        //    string connectString = string.IsNullOrEmpty(_dbURL) ? "redis://redis.qa.corva.ai:6379" : _dbURL;
        //    Console.WriteLine($"Connecting to Redis '{connectString}'...");
        //    _redis = ConnectionMultiplexer.Connect(ConfigurationOptions.Parse(connectString));
        //    _db = _redis.GetDatabase();
        //    _isConnected = true;
        //}


        public void Connect()
        {
            string connectString = string.IsNullOrEmpty(_dbURL) ? _defaultURL : _dbURL;
            Console.WriteLine($"Connecting to Redis '{connectString}'...");
            _pool = new RedisManagerPool(connectString);
            _client = _pool.GetClient();

            _isConnected = true;
        }

        public string GetState()
        {
            if (!_isConnected)
                Connect();

            return _client.Get<string>(_stateKey);
        }

        public bool SetState(string value)
        {
            if (!_isConnected)
                Connect();

            return _client.Set(_stateKey, value);
        }

        public void Dispose()
        {
            _client?.Dispose();
            _pool?.Dispose();
        }
    }
}
