namespace Witsml.SourceApp.Settings
{
    public static class DebugOutput
    {
        public static readonly string Request = "request";
        public static readonly string Xml = "xml";
        public static readonly string Output = "output";
        public static readonly string Timing = "timing";
    }
}