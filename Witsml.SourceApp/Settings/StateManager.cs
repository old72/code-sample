﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Witsml.SourceApp.ApiRequests;
using Witsml.SourceApp.Extensions;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.Settings
{
    public class StateManager
    {
        private readonly IAppSettingProvider _appSettingProvider;
        private readonly string _cacheKey = "source_app_state";
        private readonly RedisClient _redis;

        private object _locker = new object();

        private Dictionary<string, object> _state;

        public StateManager(IAppSettingProvider appSettingProvider, RedisClient redis)
        {
            _appSettingProvider = appSettingProvider;
            _redis = redis;

            //_redis.Connect();
        }

        private string fullCacheKey => _cacheKey + _appSettingProvider.AppId + _appSettingProvider.AssetId.ToString();

        private bool SaveCache(Dictionary<string, object> data)
        {
            var serializedData = JsonConvert.SerializeObject(data);

            return _redis.SetState(serializedData);
        }

        public void ReadAndUpdateConfig()
        {
            var cache = GetCache(fullCacheKey);

            if (cache == null)
            {
                _state = new Dictionary<string, object>();
                return;
            }

            _state = JsonConvert.DeserializeObject<Dictionary<string, object>>(cache);

            _appSettingProvider.UpdateSettings(_state);
        }

        private string GetCache(string key)
        {
            var stringData = _redis.GetState();

            return stringData;
        }

        public void SaveState(string name, string value)
        {
            lock (_locker)
            {
                try
                {
                    _state.Remove(name);
                    _state[name] = value;

                    // Save the changes in cache
                    SaveCache(_state);
                    //Console.WriteLine($"{name} : {value}");
                }
                catch (Exception ex)
                {
                    throw new Exception($"StateManager.SaveState with name: '{name}' exception: {ex.Message}");
                }
            }
        }
    }
}
