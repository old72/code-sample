﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Witsml.SourceApp.Settings
{
    public class ScheduleStatus
    {
        public static readonly string processing = "processing";
        public static readonly string completed = "completed";
    }
}
