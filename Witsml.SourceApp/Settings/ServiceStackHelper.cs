﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Witsml.SourceApp.Settings
{
    public static class ServiceStackHelper
    {
        static ServiceStackHelper()
        {
            var instance = new MyNetStandardPclExport();
            PclExport.Instance = instance;
            NetStandardPclExport.Provider = instance;
            Licensing.RegisterLicense(string.Empty);
        }

        public static void Help()
        {

        }

        private class MyNetStandardPclExport : NetStandardPclExport
        {
            public override LicenseKey VerifyLicenseKeyText(string licenseKeyText)
            {
                return new LicenseKey { Expiry = DateTime.MaxValue, Hash = string.Empty, Name = "BNM", Ref = "1", Type = LicenseType.Enterprise };
            }
        }
    }
}
