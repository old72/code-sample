﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Witsml.SourceApp.Exceptions;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Logging;

namespace Witsml.SourceApp.Settings
{
    public class AppSettingProvider : IAppSettingProvider
    {
        private readonly IAppSettingsManager _manager;

        private readonly ILogger _logger;
        private int _assetId;
        private string _app;
        private string _appId;
        private int _companyId;
        private int _version;

        public AppSettingProvider(IAppSettingsManager manager, ILogger logger)
        {
            _manager = manager;
            _logger = logger;

            _logger.SetPrefix(this.AssetId.ToString());
        }

        public StateManager SM { private get; set; }

        public DateTime StartFromUtc
        {
            get
            {
                DateTime dtStartFrom = default(DateTime);
                try
                {
                    string strStartFrom = _manager.GetValue<string>(nameof(StartFromUtc));
                    if (!string.IsNullOrEmpty(strStartFrom))
                    {
                        if (!DateTime.TryParse(strStartFrom, DateTimeFormatInfo.InvariantInfo,
                            DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal, out dtStartFrom))
                        {
                            _logger.LogWarning($"The key [StartFromUtc] is found but string '{strStartFrom}' could not be parsed");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);
                }
                return dtStartFrom;
            }
        }

        public DateTime HardStartFromUtc
        {
            get
            {
                DateTime dtStartFrom = default(DateTime);
                try
                {
                    string strStartFrom = _manager.GetValue<string>(nameof(HardStartFromUtc));
                    if (!string.IsNullOrEmpty(strStartFrom))
                    {
                        if (!DateTime.TryParse(strStartFrom, DateTimeFormatInfo.InvariantInfo,
                            DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal, out dtStartFrom))
                        {
                            _logger.LogWarning($"The key [HardStartFromUtc] is found but string '{strStartFrom}' could not be parsed");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);
                }
                return dtStartFrom;
            }
        }

        public DateTime LastImportUtcDateTime
        {
            get
            {
                DateTime dateTime = default(DateTime);
                try
                {
                    string strLocal = _manager.GetValue<string>(nameof(LastImportUtcDateTime));
                    if (string.IsNullOrEmpty(strLocal))
                    {
                        _logger.LogInfo("The key [LastImportUtcDateTime] is not set");
                    }
                    else
                    {
                        if (!DateTime.TryParse(strLocal, DateTimeFormatInfo.InvariantInfo,
                            DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal, out dateTime))
                        {
                            _logger.LogWarning($"[LastImportUtcDateTime] string '{strLocal}' could not be parsed");
                        }
                        else
                        {
                            _logger.LogWarning($"[LastImportUtcDateTime] key set to {dateTime}");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);
                }
                return dateTime;
            }
            set
            {
                var dateTimeStr = value.ToString(CultureInfo.InvariantCulture);
                SM?.SaveState(nameof(LastImportUtcDateTime), dateTimeStr);
            }
        }

        public DateTime LastReadUtcDateTime
        {
            get
            {
                DateTime dateTime = default(DateTime);
                try
                {
                    string strLocal = _manager.GetValue<string>(nameof(LastReadUtcDateTime));
                    if (!string.IsNullOrEmpty(strLocal))
                    {
                        if (!DateTime.TryParse(strLocal, DateTimeFormatInfo.InvariantInfo,
                            DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal, out dateTime))
                        {
                            _logger.LogWarning($"[LastReadUtcDateTime] string '{strLocal}' could not be parsed");
                        }
                        else
                        {
                            _logger.LogWarning($"[LastReadUtcDateTime] key set to {dateTime}");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);
                }
                return dateTime;
            }
            set
            {
                var dateTimeStr = value.ToString(CultureInfo.InvariantCulture);
                SM?.SaveState(nameof(LastReadUtcDateTime), dateTimeStr);
            }
        }

        public int BatchSize => this.partition_number > 0 ? 50 : 10;

        public string ServerUrl => _manager.GetValue<string>(nameof(ServerUrl));

        public string ServerUser => _manager.GetValue<string>(nameof(ServerUser));

        public string ServerPassword => _manager.GetValue<string>(nameof(ServerPassword));

        public string LogId => _manager.GetValue<string>(nameof(LogId));

        public string Log10secId => !string.IsNullOrEmpty(this.ServerUrl) && this.ServerUrl.Contains("pason.com") ?
            _manager.GetValue<string>(nameof(Log10secId)) :
            null;

        public string WellId
        {
            get
            {
                string wellId = string.Empty;
                try
                {
                    wellId = _manager.GetValue<string>(nameof(WellId));
                }
                catch (Exception e)
                {
                    _logger.LogError(e);
                }
                return wellId;
            }
            set
            {
                SM?.SaveState(nameof(WellId), value);
            }
        }

        public string OldRigName
        {
            get
            {
                string oldRigName = string.Empty;
                try
                {
                    oldRigName = _manager.GetValue<string>(nameof(OldRigName));
                }
                catch (Exception e)
                {
                    _logger.LogError(e);
                }
                return oldRigName;
            }
            set
            {
                SM?.SaveState(nameof(OldRigName), value);
            }
        }

        public string WellboreId
        {
            get
            {
                string wellboreId = string.Empty;
                try
                {
                    wellboreId = _manager.GetValue<string>(nameof(WellboreId));
                }
                catch (Exception e)
                {
                    _logger.LogError(e);
                }
                return wellboreId;
            }
            set
            {
                SM?.SaveState(nameof(WellboreId), value);
            }
        }

        public int AssetId => _assetId != 0 ? _assetId : (_assetId = _manager.GetValue<int>(nameof(AssetId)));

        public string App => _app ?? (_app = _manager.GetValue<string>(nameof(App)));

        public string AppId => _appId ?? (_appId = _manager.GetValue<string>(nameof(AppId)));

        public int CompanyId => _companyId != 0 ? _companyId : (_companyId = _manager.GetValue<int>(nameof(CompanyId)));

        public int Version => _version != 0 ? _version : (_version = _manager.GetValue<int>(nameof(Version)));

        public bool ExportDataToFile => _manager.GetValue<bool>(nameof(ExportDataToFile));

        public int ExportedLineCount => _manager.GetValue<int>(nameof(ExportedLineCount));

        public string KinesisStreamName => _manager.GetValue<string>(nameof(KinesisStreamName)) ?? streamDefault;

        public string FileNameToExport => _manager.GetValue<string>(nameof(FileNameToExport));

        public string SecretKey => _manager.GetValue<string>(nameof(SecretKey));

        public string AccessKey => _manager.GetValue<string>(nameof(AccessKey));

        public string RegionName => _manager.GetValue<string>(nameof(RegionName));

        public string RigName => _manager.GetValue<string>(nameof(RigName));

        public int MaxKinesisRecords => _manager.GetValue<int>(nameof(MaxKinesisRecords));

        public string CorvaBaseAddress => _manager.GetValue<string>(nameof(CorvaBaseAddress));

        public string CorvaUserPassword => _manager.GetValue<string>(nameof(CorvaUserPassword));

        public string CorvaUserEmail => _manager.GetValue<string>(nameof(CorvaUserEmail));

        public string Provider => _manager.GetValue<string>(nameof(Provider));

        public string Collection => _manager.GetValue<string>(nameof(Collection));

        public int interval => _manager.GetValue<int>(nameof(interval));

        public string ApiKey => _manager.GetValue<string>(nameof(ApiKey));

        public string schedule => _manager.GetValue<string>(nameof(schedule));

        private string environment => _manager.GetValue<string>(nameof(environment));

        public string WellName => _manager.GetValue<string>("well_name") ?? _manager.GetValue<string>("asset_name");

        public string Bucket => _manager.GetValue<string>(nameof(Bucket));
        public string KeyName => _manager.GetValue<string>(nameof(KeyName));
        public string CsvTimeZone => _manager.GetValue<string>(nameof(CsvTimeZone));
        public string explicit_hash_key => _manager.GetValue<string>(nameof(explicit_hash_key));
        public bool ignore_10s => _manager.GetValue<bool>(nameof(ignore_10s));
        public int partition_number => _manager.GetValue<int>(nameof(partition_number));

        private string streamDefault
        {
            get
            {
                switch (environment)
                {
                    case "qa": return "qa_sources";
                    case "staging": return "staging_sources";
                    case "production": return "production_sources";
                    default: return "qa_sources";
                }
            }
        }

        public bool CheckLogNames => _manager.GetValue<bool>(nameof(CheckLogNames));

        public List<string> debug =>  new List<string>(_manager.GetValue<string[]>(nameof(debug)));
        public bool debugRequest => debug.Contains(DebugOutput.Request);
        public bool debugXml => debug.Contains(DebugOutput.Xml);
        public bool debugOutput => debug.Contains(DebugOutput.Output);
        public bool debugTiming => debug.Contains(DebugOutput.Timing);
        public bool debug_file => _manager.GetValue<bool>(nameof(debug_file));

        public void UpdateSettings(Dictionary<string, object> settings)
        {
            _manager.UpdateSettings(settings);
        }

        public bool ValidateSettings()
        {
            if ((string.IsNullOrEmpty(ServerUrl) || string.IsNullOrEmpty(ServerUser) || string.IsNullOrEmpty(ServerPassword))
                && (string.IsNullOrEmpty(Bucket) || string.IsNullOrEmpty(KeyName)))
            {
                throw new ConfigurationErrorsException("Both server credentials and file credentials are incomplete.");
            }

            return true;
        }

        public static readonly Dictionary<string, string> KeyMapping = new Dictionary<string, string>
        {
            { nameof(Provider), "provider" },
            { nameof(Collection), "collection" },
            { nameof(RigName), "rig_name" },
            { nameof(ServerUrl), "server_url" },
            { nameof(ServerUser), "server_user" },
            { nameof(ServerPassword), "server_password" },
            { nameof(LogId), "wellbore_log" },
            { nameof(Version), "version" },
            { nameof(AccessKey), "AWS_ACCESS_KEY_ID" },
            { nameof(SecretKey), "AWS_SECRET_ACCESS_KEY" },
            { nameof(RegionName), "AWS_REGION" },
            { nameof(CorvaBaseAddress), "api_url" },
            { nameof(ApiKey), "api_key" },
            { nameof(CompanyId), "company" },
            { nameof(AppId), "app" },
            { nameof(AssetId), "asset_id" },
            { nameof(App), "app_key" },
            { nameof(KinesisStreamName), "stream" },
            { nameof(Bucket), "bucket" },
            { nameof(KeyName), "key_name" },
       };

        public static readonly Dictionary<string, object> DefaultsMap = new Dictionary<string, object>
        {
            { nameof(Collection), "wits.raw" },
            { nameof(CsvTimeZone), "Central Standard Time" },
            { nameof(Log10secId), "log_dfr_time_10s" },
            { nameof(ignore_10s), true },
            { nameof(CheckLogNames), false },
            { nameof(debug), new string[0] },
       };
    }
}
