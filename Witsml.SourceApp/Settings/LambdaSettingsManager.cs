﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Witsml.SourceApp.Interfaces;
using Newtonsoft.Json.Linq;

namespace Witsml.SourceApp.Settings
{
    internal class LambdaSettingsManager : IAppSettingsManager
    {
        private JObject _configuration;

        public LambdaSettingsManager(string config)
        {
            _configuration = new JObject();

            if (!string.IsNullOrEmpty(config))
            {
                var configJson = JObject.Parse(config);

                foreach (KeyValuePair<string, JToken> property in configJson)
                {
                    if (property.Value.Type == JTokenType.Object)
                    {
                        JObject innerJObject = property.Value.Value<JObject>();

                        foreach (KeyValuePair<string, JToken> innerProperty in innerJObject)
                        {
                            _configuration[innerProperty.Key] = innerProperty.Value;
                        }
                    }
                    else
                    {
                        _configuration[property.Key] = property.Value;
                    }
                }
            }
            else
            {
                Console.WriteLine($"Config is null.");
            }

            if (!_configuration.HasValues)
            {
                Console.WriteLine($"Config is empty.");
            }
        }

        public T GetValue<T>(string key)
        {
            string finalKey = key;
            if (AppSettingProvider.KeyMapping.TryGetValue(key, out string mappedKey))
            {
                finalKey = mappedKey;
            }

            if (_configuration.TryGetValue(finalKey, out JToken value))
            {
                if (value.Type == JTokenType.Array)
                {
                    JArray array = value as JArray;
                    return (T)array.ToObject(typeof(T));
                }
                return value.Value<T>();
            }

            return Defaults.getDefault<T>(key);
        }

        public void UpdateSettings(Dictionary<string, object> settings)
        {
            if (settings != null)
            {
                foreach (string savedKey in settings.Keys)
                {
                    _configuration[savedKey] = settings[savedKey].ToString();
                }
            }
        }
    }
}