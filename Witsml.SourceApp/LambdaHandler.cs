﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Autofac;
using Witsml.SourceApp.Logging;
using Witsml.SourceApp.Settings;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.IoC;
using System.Diagnostics;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializerAttribute(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Witsml.SourceApp
{
    class LambdaHandler
    {
        private ILogger _logger;
        private IContainer _container;
        private IReplicator _replicator;

        public static void Main()
        {
            string result = Task.Run(async () =>
            {
                using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), "config.json"), FileMode.Open))
                {
                    return await new LambdaHandler().Function(stream);
                }
            }).GetAwaiter().GetResult();

            Console.WriteLine(result);
        }

        public async Task<string> Function(Stream inputStream)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Console.Error.WriteLine("Starting Witsml.SourceApp...");

            try
            {
                ServiceStackHelper.Help();

                string input = null;
                if (inputStream != null)
                    using (var sr = new StreamReader(inputStream))
                    {
                        input = sr.ReadToEnd();
                    }
                Console.WriteLine(input);
                RedisClient._dbURL = Environment.GetEnvironmentVariable("CACHE_URL");
                Console.WriteLine("Redis URL: " + RedisClient._dbURL);

                using (_container = Bootstrapper.Init(input))
                using (var scope = _container.BeginLifetimeScope())
                {
                    _logger = scope.Resolve<ILogger>();
                    _replicator = scope.Resolve<IReplicator>();

                    var autoEvent = new AutoResetEvent(false);
                    _replicator.Start(autoEvent);

                    autoEvent.WaitOne();

                    Console.WriteLine("Stoping process...");
                }
            }
            catch (Exception ex)
            {
                _logger?.LogError($"Run import process exception occured: {ex.Message}. StackTrace: {ex.StackTrace}");
            }

            stopwatch.Stop();
            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

            return null;
        }
    }
}
