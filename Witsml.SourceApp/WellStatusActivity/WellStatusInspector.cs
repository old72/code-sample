﻿using System;
using System.Collections.Generic;
using System.Linq;
using Witsml.SourceApp.Interfaces;
using System.Diagnostics;
using Witsml.SourceApp.Exceptions;
using nwitsml.Core;
using Witsml.SourceApp.Settings;

namespace Witsml.SourceApp.WellStatusActivity
{
    public class WellStatusInspector : IWellStatusInspector
    {
        private const string WellboreIdTail = "_wb1";
        private const string WellDrillingStatus = "drilling";
        private const string WellActiveStatus = "active";
        private readonly ILogger _logger;
        private readonly IAppSettingProvider _appSettingProvider;
        private WitsmlServer _server;
        private bool _initialized;
        private bool _stopped;
        private List<WitsmlWell> _witsmlWells;
        private string _activeWellId;
        private string _activeWellboreId;
        private WitsmlWell _activeWell;
        private WitsmlWellbore _activeWellbore;


        public WellStatusInspector(ILogger logger, IAppSettingProvider appSettingProvider)
        {
            _logger = logger;
            _appSettingProvider = appSettingProvider;
        }

        public void Init(WitsmlServer server)
        {
            _server = server;
            _initialized = true;
        }

        public string ActiveWellId => _activeWell?.getId() ?? _activeWellId;

        public string ActiveWellboreId => _activeWellbore?.getId() ?? _activeWellboreId;

        public string ActiveWellName => _activeWell?.getName();

        public void Inspect()
        {
            Debug.Assert(_initialized);

            Debug.Assert(!string.IsNullOrEmpty(_appSettingProvider.RigName), "Configuration error: Rig name is not set.");

            if (string.Compare(_appSettingProvider.RigName, _appSettingProvider.OldRigName, true) != 0)
            {
                _logger.LogInfo($"Rig is changed to {_appSettingProvider.RigName}");
                _logger.LogInfo("Inspecting rig wells statuses...");

                var rigs = GetWitsmlRigs();

                if (IsGenericRig(rigs))
                {
                    _witsmlWells = _server.get<WitsmlWell>(new WitsmlQuery());
                }
                else
                {
                    // prepare wells ids and query for rig wells
                    var ids = GetWellIds(rigs);
                    _witsmlWells = _server.getMany<WitsmlWell>(new WitsmlQuery(), ids, null);
                }

                LogWellStatuses();

                WitsmlWell foundWell;
                if (string.IsNullOrEmpty(_appSettingProvider.WellName))
                {
                    foundWell = _witsmlWells.FirstOrDefault(w => IsWellActive(w.getStatus()));
                    if (foundWell == null)
                    {
                        throw new ConfigurationErrorsException("Active well not found.");
                    }
                }
                else
                {
                    foundWell = _witsmlWells.FirstOrDefault(w => string.Compare(_appSettingProvider.WellName, w.getName(), true) == 0);
                    if (foundWell == null)
                    {
                        throw new ConfigurationErrorsException($"Well '{_appSettingProvider.WellName}' is not found.");
                    }
                }

                var wellbores = _server.get<WitsmlWellbore>(new WitsmlQuery(), foundWell);
                if (!wellbores.Any())
                {
                    throw new ConfigurationErrorsException($"Could not find wellbore for well '{foundWell.getName()}'.");
                }

                LogActiveWellChange(foundWell);
                _activeWell = foundWell;
                _activeWellbore = wellbores[0];

                _appSettingProvider.OldRigName = _appSettingProvider.RigName;
                _appSettingProvider.WellId = _activeWell.getId();
                _appSettingProvider.WellboreId = _activeWellbore.getId();
            }
            else
            {
                // try to restore from cache
                Debug.Assert(!string.IsNullOrEmpty(_appSettingProvider.WellId), "Could not find Well id in cache.");
                Debug.Assert(!string.IsNullOrEmpty(_appSettingProvider.WellboreId), "Could not find Wellbore id in cache.");

                _activeWellId = _appSettingProvider.WellId;
                _activeWellboreId = _appSettingProvider.WellboreId;
            }
        }

        private bool IsGenericRig(IList<WitsmlRig> rigs)
        {
            return rigs.Count() == 1 && "1".Equals(rigs[0].getId());
        }

        public void StopInspector()
        {
            if (_stopped)
            {
                return;
            }

            _stopped = true;
        }

        private IList<WitsmlRig> GetWitsmlRigs()
        {
            IList<WitsmlRig> rigs;

            if (!String.IsNullOrEmpty(_appSettingProvider.ServerUrl) && _appSettingProvider.ServerUrl.Contains(Constants.MyWellsUrl))
            {
                rigs = new List<WitsmlRig>();
                WitsmlRig genericRig = new nwitsml.Core.v131.WitsmlRig(null, "1", "Rig1", null, null);

                rigs.Add(genericRig);

                return rigs;
            }

            // quering for rig with given name
            var rigQuery = new WitsmlQuery();
            rigQuery.addElementConstraint("name", _appSettingProvider.RigName);
            rigs = _server.get<WitsmlRig>(rigQuery);

            if (rigs.Any())
            {
                return rigs;
            }
            _logger.LogError($"Rig ['{_appSettingProvider.RigName}'] has no wells");
            throw new ConfigurationErrorsException($"Rig ['{_appSettingProvider.RigName}'] has no wells.");
        }

        private string[] GetWellIds(IList<WitsmlRig> rigs)
        {
            return rigs.Select(r => r.getWellId()).ToArray();
            //            return rigs.Select(r => WellboreIdToWellId(r.getParentId())).ToArray();
        }

        private string WellboreIdToWellId(string wellboreId)
        {
            return wellboreId.Replace(WellboreIdTail, "");
        }

        private void LogWellStatuses()
        {
            foreach (var witsmlWell in _witsmlWells)
            {
                _logger.LogInfo($"Well name: {witsmlWell.getName()}, uid: [{witsmlWell.getId()}]. Status: {witsmlWell.getStatus()}");
            }
        }

        private void LogActiveWellChange(WitsmlWell newWell)
        {
            _logger.LogInfo(_activeWell != null
                ? $"Active well changing from {_activeWell.getName()} to {newWell.getName()}"
                : $"Active well set to {newWell.getName()}");
        }

        private bool IsWellActive(string status)
        {
            return status == WellActiveStatus || status == WellDrillingStatus;
        }
    }
}
