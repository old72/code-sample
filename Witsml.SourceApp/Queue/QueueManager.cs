﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Witsml.SourceApp.Model;
using Witsml.SourceApp.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Witsml.SourceApp.Queue
{
    public class QueueManager<T>
    {
        private readonly ILogger _logger;
        private readonly BlockingCollection<T> _queue = new BlockingCollection<T>(new ConcurrentQueue<T>(), 1000);

        public QueueManager(ILogger logger)
        {
            _logger = logger;
        }

        public bool AddToQueue(T itemToAdd)
        {
            try
            {
                if (!_queue.IsAddingCompleted && !_queue.IsCompleted)
                {
                    return _queue.TryAdd(itemToAdd);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e);
            }
            return false;
        }

        public void MarkAsComplete()
        {
            _queue.CompleteAdding();
        }

        public async Task<bool> ProcessQueueItem(Func<IEnumerable<T>, RunnerStatus, Task<bool>> processFunc, RunnerStatus runnerStatus)
        {
            if (!_queue.IsCompleted)
            {
                try
                {
                    IList<T> itemsList = new List<T>();
                    bool result = false;
                    do
                    {
                        result = _queue.TryTake(out T item);
                        if (result)
                        {
                            itemsList.Add(item);
                        }
                    } while (result && itemsList.Count < 100);

                    if (itemsList.Any())
                    {
                        
                        _logger.LogInfo($"{itemsList.Count} items were taken from queue");
                        return await processFunc(itemsList, runnerStatus);
                    }
                        
                    return true;
                }
                catch (InvalidOperationException ex)
                {
                    //_logger.LogError("Queue was marked as complete for adding...");
                    _logger.LogError(ex);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);
                }
            }
            runnerStatus.Cancelled = true;
            return false;
        }
    }
}
