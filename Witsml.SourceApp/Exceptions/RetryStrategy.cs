﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Witsml.SourceApp.Exceptions
{
    public interface IRetryStrategy
    {
        bool ShouldRetry(Exception ex);
    }

    public static class RetryStrategy
    {
        public static RetryStrategy<KinesisDataProducerRetryStrategy> CreateKinesisRetryStrategy()
        {
            return new RetryStrategy<KinesisDataProducerRetryStrategy>(0, 50, 10);
        }
    }

    public class RetryStrategy<T> where T : IRetryStrategy
    {
        private int _numberOfAttempts, _initialDelay, _delayStep;
        private IRetryStrategy _retryStrategy;

        public RetryStrategy(int numberOfAttempts, int initialDelayInMillisec, int delayStepInMillisec)
        {
            _numberOfAttempts = numberOfAttempts;
            _initialDelay = initialDelayInMillisec > 0 ? initialDelayInMillisec : 0;
            _delayStep = delayStepInMillisec > 0 ? delayStepInMillisec : 0;
            _retryStrategy = Activator.CreateInstance<T>();
        }

        public void Execute(Action action)
        {
            Func<bool> func = () => { action(); return true; };
            Execute(func);
        }

        public TResult Execute<TResult>(Func<TResult> action)
        {
            int i = 0;
            Exception last = null;
            while (i < _numberOfAttempts || _numberOfAttempts == 0)
            {
                if (i != 0)
                {
                    Thread.Sleep(_initialDelay + _delayStep * (i - 1));
                    Console.WriteLine($"Retry {i}");
                }
                try
                {
                    TResult result = action();
                    return result;
                }
                catch (Exception ex)
                {
                    last = ex;
                    if (!_retryStrategy.ShouldRetry(ex))
                    {
                        throw;
                    }
                }
                i++;
            }
            throw new Exception(string.Format("Retry failed\r\n{0}", last.ToString()));
        }
    }

    public class KinesisDataProducerRetryStrategy : IRetryStrategy
    {
        public bool ShouldRetry(Exception ex)
        {
            if (ex == null)
            {
                return false;
            }
            return true;
        }
    }
}

