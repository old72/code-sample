﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Witsml.SourceApp.ApiRequests;
using Witsml.SourceApp.Exceptions;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model;
using Witsml.SourceApp.Model.ApiModel;

namespace Witsml.SourceApp.DataSenders
{
    public class MessageProducerDataSender<T> : IDataSender<T>
    {
        private readonly ILogger _logger;
        private readonly IAppSettingProvider _appSettingProvider;
        private bool _isDisposed;
        public event EventHandler<EventArgs> SenderReady;
        public event EventHandler<EventArgs> SenderDisconnected;
        private int _recordsSent;
        private RetryStrategy<KinesisDataProducerRetryStrategy> _retryStrategy;
        private int iFile = 1;

        public MessageProducerDataSender(ILogger logger, IAppSettingProvider appSettingProvider)
        {
            _logger = logger;
            _appSettingProvider = appSettingProvider;
        }

        public void Init()
        {
            _retryStrategy = RetryStrategy.CreateKinesisRetryStrategy();

            SenderReady?.Invoke(this, new EventArgs());
        }

        public async Task<bool> Send(IEnumerable<T> data, RunnerStatus sourceToken)
        {
            if (_appSettingProvider.debug_file && _appSettingProvider.debugOutput)
            {
                try
                {
                    File.WriteAllText($"output{iFile++}.xml", JsonConvert.SerializeObject(data));
                }
                catch { }
            }

            foreach (T d in data)
            {
                if (!_appSettingProvider.debug_file && _appSettingProvider.debugOutput)
                {
                    Console.WriteLine(JsonConvert.SerializeObject(d, Formatting.None));
                }

                if (!await Send(d, sourceToken))
                {
                    return false;
                }
            }

            return true;
        }

        private async Task<bool> Send(T data, RunnerStatus sourceToken)
        {
            try
            {
                if (sourceToken.Cancelled)
                {
                    return false;
                }
                if (_appSettingProvider.MaxKinesisRecords != 0 && _recordsSent >= _appSettingProvider.MaxKinesisRecords)
                {
                    SenderDisconnected?.Invoke(this, new EventArgs());
                    return false;
                }

                SendMessageRequestBody<T> requestRecord = PrepareRequestBody(data);

                SendMessageRequest<T> sendMessageRequest = new SendMessageRequest<T>(_appSettingProvider.App, requestRecord);

                var sendResult = _retryStrategy.Execute(() =>
                    {
                        ApiResponse<object> result = Task.Run(async () =>
                        {
                            return await sendMessageRequest.ExecuteAsync();
                        }).GetAwaiter().GetResult();

                        return result;
                    });

                return AnalyzeResult(sendResult);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }

        private bool AnalyzeResult(ApiResponse<object> result)
        {
            if (!result.HttpResponseMessage.IsSuccessStatusCode)
            {
                _logger.LogInfo($"Not successful send message: result message = {result.HttpResponseMessage}");
                return false;
            }
            _logger.LogInfo($"Successfully sent message.");
            _recordsSent++;

            return true;
        }

        private SendMessageRequestBody<T> PrepareRequestBody(T data)
        {
            var requestRecord = new SendMessageRequestBody<T>
            {
                Asset_id = _appSettingProvider.AssetId,
                Data = data,
            };

            var dataBytes = PrepareBytes(data);
            _logger.LogInfo($"Size of data: { (dataBytes.Length / 1024.0):0.00} KiB ");

            return requestRecord;
        }

        private static byte[] PrepareBytes(T data)
        {
            var serializedData = JsonConvert.SerializeObject(data);
            return Encoding.UTF8.GetBytes(serializedData);
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                SenderDisconnected?.Invoke(this, new EventArgs());
                SenderDisconnected = null;
                _isDisposed = true;
            }
        }
    }
}
