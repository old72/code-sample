﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model;
using Witsml.SourceApp.Logging;
using System.Collections.Generic;

namespace Witsml.SourceApp.DataSenders
{
    public class ExportToFileDataSender<T> : IDataSender<T>, IDisposable
    {
        private readonly string _fileNameToExport;
        private readonly ILogger _logger;
        private readonly IAppSettingProvider _appSettingProvider;
        private bool _isDisposed;
        private StreamWriter _streamWriter;
        FileStream _fileStream;
        private int _alreadyExportedLines;

        public event EventHandler<EventArgs> SenderReady;
        public event EventHandler<EventArgs> SenderDisconnected;


        public ExportToFileDataSender(string fileNameToExport, ILogger logger, IAppSettingProvider appSettingProvider)
        {
            _fileNameToExport = fileNameToExport;
            _logger = logger;
            _appSettingProvider = appSettingProvider;
        }

        public void Init()
        {
            try
            {
                _fileStream = File.Create(_fileNameToExport);
                _streamWriter = new StreamWriter(_fileStream);
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                return;
            }
            
            SenderReady?.Invoke(this, new EventArgs());
        }

        public async Task<bool> Send(IEnumerable<T> data, RunnerStatus sourceToken)
        {
            if (_appSettingProvider.ExportedLineCount <= _alreadyExportedLines)
            {
                await _streamWriter.FlushAsync();
                SenderDisconnected?.Invoke(this, new EventArgs());
                return await Task.FromResult(true);
            }
            try
            {
                foreach (T d in data)
                {
                    var serializedData = JsonConvert.SerializeObject(d);
                    await _streamWriter.WriteLineAsync(serializedData);
                    _alreadyExportedLines++;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                return await Task.FromResult(false);
            }

            return await Task.FromResult(true);
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _streamWriter?.Flush();
                _streamWriter?.Dispose();
                _fileStream?.Dispose();
                SenderReady = null;
                _isDisposed = true;
            }
        }
    }
}
