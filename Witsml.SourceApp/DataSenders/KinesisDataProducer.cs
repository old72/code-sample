﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using Amazon.Runtime;
using Newtonsoft.Json;
using Witsml.SourceApp.Exceptions;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model;

namespace Witsml.SourceApp.DataSenders
{
    public class KinesisDataProducer<T> : IDataSender<T>
    {
        private readonly ILogger _logger;
        private readonly IAppSettingProvider _appSettingProvider;
        private AmazonKinesisClient _kinesisClient;
        private bool _isDisposed;
        public event EventHandler<EventArgs> SenderReady;
        public event EventHandler<EventArgs> SenderDisconnected;
        private int _recordsSent;
        private RetryStrategy<KinesisDataProducerRetryStrategy> _retryStrategy;

        public KinesisDataProducer(ILogger logger, IAppSettingProvider appSettingProvider)
        {
            _logger = logger;
            _appSettingProvider = appSettingProvider;
        }

        public void Init()
        {
            try
            {
                var credentials = new BasicAWSCredentials(_appSettingProvider.AccessKey, _appSettingProvider.SecretKey);
                var regionEndpoint = RegionEndpoint.GetBySystemName(_appSettingProvider.RegionName);
                _kinesisClient = new AmazonKinesisClient(credentials, regionEndpoint);

                _kinesisClient.ExceptionEvent += _kinesisClient_ExceptionEvent;
                _retryStrategy = RetryStrategy.CreateKinesisRetryStrategy();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                throw;
            }
            SenderReady?.Invoke(this, new EventArgs());
        }

        private void _kinesisClient_ExceptionEvent(object sender, ExceptionEventArgs e)
        {
            _logger.LogError($"Kinesis exception event: {e}");
        }

        public async Task<bool> Send(IEnumerable<T> data, RunnerStatus sourceToken)
        {
            try
            {
                if (sourceToken.Cancelled)
                {
                    return false;
                }
                if (_appSettingProvider.MaxKinesisRecords != 0 && _recordsSent >= _appSettingProvider.MaxKinesisRecords)
                {
                    SenderDisconnected?.Invoke(this, new EventArgs());
                    return false;
                }

                if (!data.Any())
                    return true;

                PutRecordsRequest requestRecord = PrepareRecordsRequest(data);
                var putResult = await

                _retryStrategy.Execute(() =>
                {
                    return _kinesisClient.PutRecordsAsync(requestRecord);
                });

                AnalyzeResult(putResult );

                return putResult.FailedRecordCount < data.Count();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }

        private bool AnalyzeResult_old(PutRecordResponse putResult, PutRecordRequest putRequest)
        {
            if (putResult.HttpStatusCode != HttpStatusCode.OK)
            {
                _logger.LogInfo($"Not successfull putrecord: partition key = {putRequest.PartitionKey}, result HttpStatusCode = {putResult.HttpStatusCode}");
                return false;
            }
            _logger.LogInfo($"Successfully putrecord: partition key = {putRequest.PartitionKey}, explicit hash key = {putRequest.ExplicitHashKey}, shard ID = {putResult.ShardId}");
            _recordsSent++;
            return true;
        }

        private void AnalyzeResult(PutRecordsResponse putResult)
        {
            foreach (PutRecordsResultEntry entry in putResult.Records)
            {
                if (string.IsNullOrEmpty(entry.ErrorCode))
                {
                    _logger.LogInfo($"Successfully putrecord: SequenceNumber = {entry.SequenceNumber}, shard ID = {entry.ShardId}");
                    _recordsSent++;
                }
                else
                {
                    _logger.LogInfo($"Not successfull putrecord: ErrorCode = {entry.ErrorCode}, ErrorMessage = {entry.ErrorMessage}");
                }
            }
        }

        private PutRecordsRequest PrepareRecordsRequest(IEnumerable<T> data)
        {
            var requestRecord = new PutRecordsRequest
            {
                StreamName = _appSettingProvider.KinesisStreamName,
                Records = data.Select(d => PrepareRecord(d)).ToList(),
            };

            return requestRecord;
        }

        private PutRecordsRequestEntry PrepareRecord(T data)
        {
            PutRecordsRequestEntry entry = new PutRecordsRequestEntry
            {
                Data = new MemoryStream(PrepareBytes(data)),
                PartitionKey = _appSettingProvider.AssetId.ToString()
            };
            _logger.LogInfo($"Size of data: {(entry.Data.Length / 1024.0):0.00} KiB ");

            if (!string.IsNullOrEmpty(_appSettingProvider.explicit_hash_key))
            {
                entry.ExplicitHashKey = _appSettingProvider.explicit_hash_key;
            }

            return entry;
        }

        private static byte[] PrepareBytes(T data)
        {
            var serializedData = JsonConvert.SerializeObject(data);
            return Encoding.UTF8.GetBytes(serializedData);
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                SenderDisconnected?.Invoke(this, new EventArgs());
                SenderDisconnected = null;
                _kinesisClient?.Dispose();
                _isDisposed = true;
            }
        }
    }
}
