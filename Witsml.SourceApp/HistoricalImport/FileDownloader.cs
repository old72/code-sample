﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using Shared.Logging;
using Witsml.SourceApp.Interfaces;

namespace Witsml.SourceApp.HistoricalImport
{
    public class FileDownloader: IFileDownloader
    {
        public event EventHandler<EventArgs> DownloadComplete;

        private readonly ILogger _logger;

        public string FilePath { get; set; }

        public FileDownloader(ILogger logger)
        {
            _logger = logger;
        }

        public void DownloadFromUrl(string url)
        {
            FilePath = Path.GetTempFileName();
            try
            {
                DownloadFileAsync(url);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
            }
        }

        protected virtual void OnDownloadComplete()
        {
            DownloadComplete?.Invoke(this, EventArgs.Empty);
        }

        async void DownloadFileAsync(string url)
        {
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri(baseUrl);
                client.Timeout = TimeSpan.FromMinutes(5);

                var request = new HttpRequestMessage(HttpMethod.Post, url);
                var sendTask = client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                var response = sendTask.Result.EnsureSuccessStatusCode();
                var httpStream = await response.Content.ReadAsStreamAsync();

                using (var fileStream = File.Create(FilePath))
                using (var reader = new StreamReader(httpStream))
                {
                    httpStream.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
            _logger.LogInfo("File download completed");
            OnDownloadComplete();
        }
    }
}
