﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model;

namespace Witsml.SourceApp.HistoricalImport
{
    public class CsvReader : IDisposable, IFileReader
    {
        private readonly ILogger _logger;
        private readonly IAppSettingProvider _appSettingProvider;
        private const string DateTimeColumnKey = "DateTime";
        private const string TimestampColumnKey = "Timestamp";
        private const string AnotherDateTimeColumnKey = "Date Time";
        private const string DateColumnKey = "\"Date\"";
        private const string SeparateDateColumnKey = "YYYY/MM/DD";
        private const string SeparateTimeColumnKey = "HH:MM:SS";
        private readonly long _epochTicks = new DateTime(1970, 1, 1).Ticks;
        private readonly TimeZoneInfo _csvTimeZone;

        private IDataReader _dataReader;
        private List<string> _headers;
        private readonly CultureInfo _usCulture = new CultureInfo("en-US");
        private string _currentLine;
        private List<string> _csvLines;
        private int _dateTimeColumnIndex = -1;
        private int _secondDateTimeColumnIndex = -1;
        private DateTime _lastUsedDatetime; // UTC
        private bool _isCompletedAdded;

        public CsvReader(ILogger logger, IAppSettingProvider appSettingProvider, IDataReader dataReader)
        {
            _logger = logger;
            _appSettingProvider = appSettingProvider;
            _dataReader = dataReader;

            var timezones = TimeZoneInfo.GetSystemTimeZones();
            _csvTimeZone = timezones.FirstOrDefault(tz => string.Compare(tz.Id, _appSettingProvider.CsvTimeZone, true) == 0);
            if (_csvTimeZone == null)
            {
                _csvTimeZone = TimeZoneInfo.FindSystemTimeZoneById("CST6CDT");
            }
            _isCompletedAdded = false;
        }

        public bool Init(string filePath)
        {
            try
            {
                if (!_dataReader.Init())
                {
                    _logger.LogError("Data reader initialization error.");
                    return false;
                }

                var firstLine = _dataReader.ReadLine();
                if (string.IsNullOrWhiteSpace(firstLine))
                {
                    _logger.LogError("Failed to read data headers at the first line.");
                    return false;
                }

                _headers = firstLine.Split(',').Select(x => x.Trim()).ToList();

                if (!_headers.Any())
                {
                    _logger.LogError("Data headers not found in historical data file.");
                    return false;
                }

                var headersDistinct = _headers.Distinct();
                if (headersDistinct.Count() != _headers.Count)
                {
                    _logger.LogWarning("Duplicate header in historical file found.");
                }

                var culumnIndex = GetDateColumnIndex();
                if (culumnIndex != -1)
                {
                    _dateTimeColumnIndex = culumnIndex;
                }
                else
                {
                    _dateTimeColumnIndex = _headers.FindIndex(x => x.Equals(SeparateDateColumnKey));
                    _secondDateTimeColumnIndex = _headers.FindIndex(x => x.Equals(SeparateTimeColumnKey));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
            return true;
        }

        public DateTime GetLineDateTime(string line)
        {
            DateTime result = default(DateTime);

            if (!string.IsNullOrEmpty(line))
            {
                var dataCells = line.Split(',').Select(x => x.Trim()).ToArray();
                if (_dateTimeColumnIndex >= 0 && _dateTimeColumnIndex < dataCells.Count())
                {
                    result = _secondDateTimeColumnIndex != -1
                        ? ConvertFromSeparateDateTimeColumns(dataCells[_dateTimeColumnIndex], dataCells[_secondDateTimeColumnIndex])
                        : ConvertToDateFromString(dataCells[_dateTimeColumnIndex]);
                    
                    result = TimeZoneInfo.ConvertTime(result, _csvTimeZone, TimeZoneInfo.Utc);
                }
            }

            return result;
        }

        private int GetDateColumnIndex()
        {
            var dateTimeColumnIndex = _headers.FindIndex(x => x.Equals(DateTimeColumnKey));
            var timestampColumnIndex = _headers.FindIndex(x => x.Equals(TimestampColumnKey));
            var dateColumnIndex = _headers.FindIndex(x => x.Equals(DateColumnKey));
            var anotherDateTimeColumnIndex = _headers.FindIndex(x => x.Equals(AnotherDateTimeColumnKey));

            var culumnIndex = -1;

            if (dateTimeColumnIndex >= 0)
            {
                culumnIndex = dateTimeColumnIndex;
            }
            else if (timestampColumnIndex >= 0)
            {
                culumnIndex = timestampColumnIndex;
            }
            else if (dateColumnIndex >= 0)
            {
                culumnIndex = dateColumnIndex;
            }
            else if (anotherDateTimeColumnIndex >= 0)
            {
                culumnIndex = anotherDateTimeColumnIndex;
            }
            return culumnIndex;
        }

        public bool ReadNextDataBatch(DateTime startDateTimeIndex)
        {
            _lastUsedDatetime = startDateTimeIndex;

            var rowsRead = 0;
            if (string.IsNullOrEmpty(_currentLine))
            {
                _currentLine = _dataReader.ReadLine();
                rowsRead++;
            }

            DateTime dt = GetLineDateTime(_currentLine);

            while (!string.IsNullOrEmpty(_currentLine) && dt < startDateTimeIndex)
            {
                try
                {
                    _currentLine = _dataReader.ReadLine();
                    dt = GetLineDateTime(_currentLine);
                    rowsRead++;
                }
                catch (Exception)
                {
                    _logger.LogError($"Exception at line: {rowsRead}");
                }
            }

            _csvLines = new List<string>();
            rowsRead = 0;
            while (!string.IsNullOrEmpty(_currentLine) && rowsRead < _appSettingProvider.BatchSize)
            {
                DateTime lineDatetime = GetLineDateTime(_currentLine);
                if (lineDatetime != default(DateTime)) // valid complete line
                {
                    _csvLines.Add(_currentLine);
                    _lastUsedDatetime = lineDatetime;
                    rowsRead++;
                }
                
                _currentLine = _dataReader.ReadLine();
            }

            if (!_csvLines.Any() && !_isCompletedAdded)
            {
                _csvLines.Add(string.Empty);
                _isCompletedAdded = true;
            }

            return _csvLines.Any();
        }

        public List<WitsmlDtoBase> GetWitsmlData(int startIndex, int rowsToRead)
        {
            var list = new List<WitsmlDtoBase>();
            for (int i = startIndex; i < _csvLines.Count && i < rowsToRead; i++)
            {
                if (!string.IsNullOrEmpty(_csvLines[i]))
                    list.Add(ToWits(_csvLines[i]));
                else
                {
                    list.Add(CreateCompleted());
                    _logger.LogInfo($"Added completed marker");
                }
            }

            return list;
        }

        public WitsmlDtoBase ToWits(string inputLine)
        {
            var dataCells = inputLine.Split(',').Select(x => x.Trim()).ToArray();

            var lineDateTime = GetLineDateTime(inputLine);

            long unixTimestamp = (lineDateTime.Ticks - _epochTicks) / TimeSpan.TicksPerSecond;
            var record = GetWitsmlDataItems(dataCells, unixTimestamp);

            return new WitsmlDtoBase
            {
                RecordTime = lineDateTime,
                Data = record,
                App = _appSettingProvider.App,
                Version = _appSettingProvider.Version,
                Asset_id = _appSettingProvider.AssetId,
                Company_id = _appSettingProvider.CompanyId,
                Timestamp = unixTimestamp,
                Collection = _appSettingProvider.Collection,
                Provider = _appSettingProvider.Provider
            };
        }

        private WitsmlDtoBase CreateCompleted()
        {
            DateTime lineDateTime = _lastUsedDatetime;

            long unixTimestamp = (lineDateTime.Ticks - _epochTicks) / TimeSpan.TicksPerSecond;

            return new WitsmlDtoBase
            {
                RecordTime = lineDateTime,
                App = _appSettingProvider.App,
                Version = _appSettingProvider.Version,
                Asset_id = _appSettingProvider.AssetId,
                Company_id = _appSettingProvider.CompanyId,
                Timestamp = unixTimestamp,
                Collection = "wits.completed",
                Provider = _appSettingProvider.Provider
            };
        }

        public DateTime GetDateTimeByIndex(int index)
        {
            return GetLineDateTime(_csvLines[index]);
        }

        public int GetRowCount()
        {
            return _csvLines.Count;
        }

        public void Dispose()
        {
            _dataReader?.Dispose();
        }

        private DateTime ConvertFromSeparateDateTimeColumns(string dateString, string timeString)
        {
            DateTime foundDate = ConvertToDateFromString(dateString);
            DateTime foundTime = ConvertToDateFromString(timeString);
            return new DateTime(foundDate.Year, foundDate.Month, foundDate.Day, foundTime.Hour, foundTime.Minute, foundTime.Second);
        }

        private DateTime ConvertToDateFromString(string value)
        {
            return Convert.ToDateTime(value, _usCulture);
        }

        private JObject GetWitsmlDataItems(string[] dataCells, long unixTimestamp = -1)
        {
            var json = new JObject();

            if (unixTimestamp != -1)
            {
                json.Add("entry_at", unixTimestamp);
            }

            for (int i = 0; i < dataCells.Length && i < _headers.Count; i++)
            {
                if (i != _dateTimeColumnIndex && i != _secondDateTimeColumnIndex)
                {
                    string description = _headers[i].ToLower();
                    if (json[description] == null)
                    {
                        json.Add(description, GetTypedValue(dataCells[i]));
                    }
                }
            }

            return json;
        }

        private JValue GetTypedValue(string value)
        {
            long longValue;
            if (long.TryParse(value, out longValue))
            {
                return new JValue(longValue);
            }

            double doubleValue;
            if (double.TryParse(value, out doubleValue))
            {
                return new JValue(doubleValue);
            }

            return new JValue(value);
        }

        public DateTime GetNextDateTime(DateTime lastIndex)
        {
            return lastIndex.AddSeconds(1);
        }
    }
}
