﻿using System;
using System.Collections.Generic;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model;
using Witsml.SourceApp.Logging;
using System.Threading;

namespace Witsml.SourceApp.DataRetrievers
{
    public class HistoricalDataRetriever : IDataRetriever
    {
        private readonly IAppSettingProvider _appSettingProvider;
        private IFileReader _fileReader;
        private readonly ILogger _logger;
        private bool _initialized;
        private bool _stopped;

        public event EventHandler<EventArgs> DataRetrieveComplete;
        public event EventHandler<EventArgs> DataRetrieverReady;
        public bool IsCompleted { get; private set; }

        public HistoricalDataRetriever(IAppSettingProvider appSettingProvider, IFileReader fileReader, ILogger logger)
        {
            _appSettingProvider = appSettingProvider;
            _fileReader = fileReader;
            _logger = logger;
        }

        public void Connect()
        {
            if (!_fileReader.Init(null))
            {
                _logger.LogError("Error during initialization reading from historical file");
                return;
            }
            _initialized = true;

            OnDataRetrieverReady();
        }

        public bool GetWellData(DateTime startDateTimeIndex, CancellationToken token)
        {
            if (!_initialized)
            {
                return false;
            }

            if (!_fileReader.ReadNextDataBatch(startDateTimeIndex))
            {
                IsCompleted = true;
                OnDataRetrieveComplete();
                return false;
            }

            return true;
        }

        public List<WitsmlDtoBase> GetWitsmlData(int startIndex, int rowsToRead)
        {
            return _fileReader.GetWitsmlData(startIndex, rowsToRead);
        }

        public int GetRowCount()
        {
            return _fileReader.GetRowCount();
        }

        public DateTime GetDateTimeByIndex(int index)
        {
            return _fileReader.GetDateTimeByIndex(index);
        }

        public void Stop()
        {
            if (_stopped)
            {
                return;
            }

            _fileReader.Dispose();
            _fileReader = null;
            DataRetrieverReady = null;
            DataRetrieveComplete = null;
            _stopped = true;
        }

        protected virtual void OnDataRetrieverReady()
        {
            DataRetrieverReady?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnDataRetrieveComplete()
        {
            DataRetrieveComplete?.Invoke(this, EventArgs.Empty);
        }

        public DateTime GetNextStartIndex(DateTime lastIndex)
        {
            return _fileReader.GetNextDateTime(lastIndex);
        }

        public int GetWaitSeconds()
        {
            return 0;
        }
    }
}
