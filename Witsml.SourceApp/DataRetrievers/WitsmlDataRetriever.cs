﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Security;
using Newtonsoft.Json.Linq;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model;
using nwitsml.Core;
using System.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace Witsml.SourceApp.DataRetrievers
{
    public class WitsmlDataRetriever : IDataRetriever
    {
        private readonly int MAX_NUMBER_OF_EMPTY_LOGS = 10;
        private readonly ILogger _logger;
        private readonly IAppSettingProvider _appSettingProvider;
        private readonly IWellStatusInspector _wellStatusInspector;
        private WitsmlServer _server;
        private bool _initialized;
        private readonly long _epochTicks = new DateTime(1970, 1, 1).Ticks;
        private bool _stopped;
        private WitsmlWellModel _model;
        //        private int _timestep;
        private int _emptyLogCounter;
        private WitsmlLog _nextWitsmlLog;
        private List<string> _10SecCurves;
        private bool _10SecCurvesLogged;
        private bool _isLog10Present;

        private Dictionary<string, int> _logTimestepsMap;
        private List<string> requestList;
        private List<string> logList;
        private Stopwatch _stopwatch;
        private int iLog;

        public event EventHandler<EventArgs> DataRetrieverReady;
        public event EventHandler<EventArgs> DataRetrieveComplete = null;
        public bool IsCompleted { get; set; }

        public WitsmlDataRetriever(ILogger logger, IAppSettingProvider appSettingProvider, IWellStatusInspector wellStatusInspector)
        {
            _logger = logger;
            _appSettingProvider = appSettingProvider;
            _wellStatusInspector = wellStatusInspector;
            _logTimestepsMap = new Dictionary<string, int>();
        }

        public void Connect()
        {
            if (_initialized)
            {
                return;
            }

            var capabilities = new Capabilities(WitsmlVersion.VERSION_1_3_1, "Ryan Dawson", "support@corva.ai",
                "5125070072", "", "Corva Platform", "Corva", "1.0");

            if (_appSettingProvider.debugRequest)
            {
                requestList = new List<string>();
            }

            if (_appSettingProvider.debugXml)
            {
                logList = new List<string>();
            }

            _server = new WitsmlServer(_appSettingProvider.ServerUrl, _appSettingProvider.ServerUser, _appSettingProvider.ServerPassword,
                WitsmlVersion.VERSION_1_3_1, capabilities, logList, requestList);
            _wellStatusInspector.Init(_server);
            _wellStatusInspector.Inspect();

            _initialized = true;
            OnDataRetrieverReady();
        }

        public bool GetWellData(DateTime startDateTimeIndex, CancellationToken token)
        {
            if (!_initialized)
            {
                _logger.LogInfo("WitsmlDataRetriever is not initialized");
                IsCompleted = true;
                return false;
            }

            if (_appSettingProvider.CheckLogNames)
            {
                if (!CheckLogNames())
                {
                    IsCompleted = true;
                    return false;
                }
            }
            else
            {
                _isLog10Present = !string.IsNullOrEmpty(_appSettingProvider.Log10secId);
            }

            string log2Id, baseLogId;

            if (_isLog10Present)
            {
                baseLogId = _appSettingProvider.Log10secId;
                log2Id = _appSettingProvider.LogId;
            }
            else
            {
                baseLogId = _appSettingProvider.LogId;
                log2Id = null;
            }

            var dateTime = startDateTimeIndex == default(DateTime) ? "start date" : startDateTimeIndex.ToString(CultureInfo.InvariantCulture);

            // retrieving data from base log (processing server stuck)
            _logger.LogInfo($"Getting data '{baseLogId}' from {dateTime}, for wellId: {_wellStatusInspector.ActiveWellId} {TryGetWellNameTail()}");

            WitsmlLog firstLog = GetWitsmlLog(startDateTimeIndex, default(DateTime), baseLogId);

            if (IsEmpty(firstLog))
            {
                if (!_isLog10Present || !_appSettingProvider.ignore_10s)
                {
                    _logger.LogWarning($"Model for logId: {baseLogId}, from startDateTimeIndex: {dateTime} is null.");

                    if (!ProcessServerStuck(startDateTimeIndex, baseLogId, token))
                    {
                        //_logger.LogWarning($"Server lag detected. Trying to get data from {startDateTimeIndex}...");
                        return false;
                    }

                    firstLog = _nextWitsmlLog;
                }
            }
            if (!IsEmpty(firstLog))
            {
                _logger.LogInfo($"Got data '{baseLogId}' from {firstLog.getStartIndex()} (endIndex: {getEndIndex(firstLog)}). RowCount: {firstLog.getIndexCurve().getNValues()}.");
                getTimestep(firstLog, baseLogId);
            }
            else
            {
                _logger.LogWarning($"Empty log data '{baseLogId}' ignored.");
            }
            // end of base log

            List<WitsmlLog> sec10Logs = null;
            Dictionary<int, IndexedDateTime> combinedLog = null;
            WitsmlLog log2 = null;

            if (log2Id != null)
            {
                DateTime endDateTime = _isLog10Present ? (IsEmpty(firstLog) ? default(DateTime) : getEndIndex(firstLog)) : default(DateTime);

                _logger.LogInfo($"Getting data '{log2Id}' from {dateTime}, for wellId: {_wellStatusInspector.ActiveWellId} {TryGetWellNameTail()}");
                log2 = GetWitsmlLog(startDateTimeIndex, endDateTime, log2Id);

                if (log2 == null)
                {
                    _logger.LogError($"No log data for '{log2Id}' from {startDateTimeIndex} to {endDateTime}.");
                    return false;
                }

                _logger.LogInfo($"Got data from {log2.getStartIndex()} (endIndex: {getEndIndex(log2)}). RowCount: {log2.getIndexCurve().getNValues()}.");
                getTimestep(log2, log2Id);

                sec10Logs = firstLog == null ? null : new List<WitsmlLog> { firstLog };

                combinedLog = CreateCombinedLog(log2, sec10Logs);

                // var existing10Sec = combinedLog.Values.Where(v => v != null);

                if (!_10SecCurvesLogged)
                {
                    if (firstLog != null)
                    {
                        // logging additional columns
                        List<string> baseLogCurves = log2.getCurves().Select(c => c.getName().ToLower()).ToList();
                        _10SecCurves = firstLog.getCurves().Select(c => c.getName().ToLower()).Where(c => !baseLogCurves.Contains(c)).ToList();
                        _logger.LogInfo($"10 sec columns: {JsonConvert.SerializeObject(_10SecCurves, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })}");
                        _10SecCurvesLogged = true;
                    }
                }
            }

            WitsmlLog witsmlLog = _isLog10Present ? log2 : firstLog;
            _model = new WitsmlWellModel
            {
                WitsmlLog = witsmlLog,
                Sec10Logs = sec10Logs?.ToList(),
                CombinedDictionary = combinedLog,
                WitsmlIndexLogCurve = witsmlLog.getIndexCurve(),
            };

            return true;
        }

        private static DateTime getEndIndex(WitsmlLog log)
        {
            if (log.getIndexCurve() == null)
                return default(DateTime);

            if (log.getIndexCurve().getValues() == null || !log.getIndexCurve().getValues().Any())
                return default(DateTime);

            return (DateTime)log.getIndexCurve().getValues().Last();
        }

        public static Dictionary<int, IndexedDateTime> CreateCombinedLog(WitsmlLog witsmlLog, IEnumerable<WitsmlLog> sec10Logs)
        {
            if (sec10Logs == null || !sec10Logs.Any())
                return null;

            IEnumerable<IndexedDateTime> witsmlLogValues = IndexArray(new List<WitsmlLog> { witsmlLog });

            IEnumerable<IndexedDateTime> sec10LogValues = IndexArray(sec10Logs);

            var combinedLog = witsmlLogValues.GroupJoin(sec10LogValues, l => l.Ticks, r => r.Ticks, (l, r) => new
            {
                Log = l,
                Sec10Logs = r
            })
            .Where(l => l.Log.Timestamp != null)
            .SelectMany(r => r.Sec10Logs.DefaultIfEmpty(), (l, r) => new CombinedLog
            {
                Index = l.Log.Index,
                Log10Sec = r
            }).ToList();

            if (combinedLog.Any() && combinedLog.Any(cl => cl.Log10Sec != null))
            {
                IndexedDateTime current10SecLog = combinedLog.First(cl => cl.Log10Sec != null).Log10Sec;

                combinedLog.ForEach(lg =>
                {
                    if (lg.Log10Sec == null)
                    {
                        lg.Log10Sec = current10SecLog;
                    }
                    else
                    {
                        current10SecLog = lg.Log10Sec;
                    }
                });
            }
            //            var emptyLog = combinedLog.Where(fl => fl.Log10Sec == null).ToList();

            return combinedLog.ToDictionary(cl => cl.Index, cl => cl.Log10Sec);
        }

        private static IEnumerable<IndexedDateTime> IndexArray(IEnumerable<WitsmlLog> witsmlLogs)
        {
            int iLog = 0;

            foreach (var log in witsmlLogs)
            {
                int i = 0;
                foreach (var v in log.getIndexCurve().getValues())
                {
                    yield return new IndexedDateTime
                    {
                        Index = i++,
                        LogNumber = iLog,
                        Timestamp = (v as DateTime?),
                        Ticks = (v as DateTime?).GetValueOrDefault().Ticks
                    };
                }
                iLog++;
            }
        }

        private bool ProcessServerStuck(DateTime startDateTime, string logId, CancellationToken token)
        {
            DateTime utcNow = DateTime.UtcNow;

            if ((utcNow - startDateTime).TotalMinutes <= 60) // we get recent data
            {
                return false;
            }
            _emptyLogCounter++;

            if (_emptyLogCounter < MAX_NUMBER_OF_EMPTY_LOGS)
            {
                _logger.LogWarning($"Retries count: {(MAX_NUMBER_OF_EMPTY_LOGS - _emptyLogCounter)}.");
                return false;
            }

            _emptyLogCounter = 0;
            bool realTime = false;

            do
            {
                if (token.IsCancellationRequested)
                {
                    _logger.LogInfo($"Cancellation requested during ProcessServerStuck.");
                    break;
                }

                if ((utcNow - startDateTime).TotalMinutes <= 60)
                {
                    realTime = true;
                    _logger.LogError($"No data on server until {startDateTime}.");
                    break;
                }

                DateTime minDateTime = new DateTime(DateTime.Today.Year, 1, 1);
                startDateTime = startDateTime == default(DateTime) ? minDateTime : startDateTime.AddHours(12);
                _logger.LogWarning($"Server lag detected. Trying to get data from {startDateTime}...");

                _nextWitsmlLog = GetWitsmlLog(startDateTime, default(DateTime), logId);
                _appSettingProvider.LastReadUtcDateTime = startDateTime;
            } while (IsEmpty(_nextWitsmlLog));

            return !IsEmpty(_nextWitsmlLog);
        }

        private bool IsEmpty(WitsmlLog log)
        {
            return log == null || !log.getCurves().Any() || log.getCurves().First().getNValues() == 0;
        }

        public List<WitsmlDtoBase> GetWitsmlData(int startIndex, int rowsToRead)
        {
            var rowsCount = GetRowCount();
            var records = new List<WitsmlDtoBase>();
            for (int index = startIndex; index < startIndex + rowsToRead && index < rowsCount; index++)
            {
                var timestamp = (DateTime)_model.WitsmlIndexLogCurve.getValue(index);
                var unixTimestamp = (timestamp.Ticks - _epochTicks) / TimeSpan.TicksPerSecond;
                var dataItem = GetWitsmlDataItems(index, unixTimestamp);
                records.Add(BuildRecord(dataItem, unixTimestamp, timestamp));
            }
            return records;
        }

        public int GetRowCount()
        {
            return _model?.WitsmlIndexLogCurve.getNValues() ?? 0;
        }

        public DateTime GetDateTimeByIndex(int endIndex)
        {
            return (DateTime)_model.WitsmlIndexLogCurve.getValue(endIndex);
        }

        public void Stop()
        {
            if (_stopped)
            {
                return;
            }

            _wellStatusInspector?.StopInspector();
            _server = null;
            DataRetrieverReady = null;
            _stopped = true;
        }

        private string TryGetWellNameTail()
        {
            return string.IsNullOrEmpty(_wellStatusInspector.ActiveWellName) ? string.Empty : "well name: " + _wellStatusInspector.ActiveWellName;
        }

        protected virtual void OnDataRetrieverReady()
        {
            DataRetrieverReady?.Invoke(this, EventArgs.Empty);
        }

        private WitsmlLog GetWitsmlLog(DateTime startDateTimeIndex, DateTime endDateTimeIndex, string logId)
        {
            var query = new WitsmlQuery();
            if (startDateTimeIndex != default(DateTime))
            {
                query.addElementConstraint("startDateTimeIndex", startDateTimeIndex);
            }
            else
            {
                query.addElementConstraint("startDateTimeIndex", DateTime.Today.AddYears(-1));
            }

            if (endDateTimeIndex != default(DateTime))
            {
                query.addElementConstraint("endDateTimeIndex", endDateTimeIndex);
            }

            query.addAttributeConstraint("log", "uidWell", _wellStatusInspector.ActiveWellId);
            query.addAttributeConstraint("log", "uidWellbore", _wellStatusInspector.ActiveWellboreId);
            query.addElementConstraint("dataRowCount", 1000);

            if (_appSettingProvider.debugTiming)
                _stopwatch = Stopwatch.StartNew();

            var witsmlLog = _server.getOne<WitsmlLog>(query, logId);

            if (_appSettingProvider.debugTiming)
            {
                _stopwatch.Stop();
                _logger.LogInfo($"Log query time: {_stopwatch.Elapsed}");
            }

            iLog++;
            if (_appSettingProvider.debugRequest)
            {
                try
                {
                    foreach (string xmlDoc in requestList)
                    {
                        if (_appSettingProvider.debug_file)
                            File.WriteAllText($"logRequest{iLog}.xml", xmlDoc);
                        else
                        {
                            string formatted = xmlDoc;

                            if (!string.IsNullOrEmpty(xmlDoc))
                            {
                                formatted = Regex.Replace(xmlDoc, @"\n", "");
                                formatted = Regex.Replace(formatted, @"\r", "");
                            }
                            _logger.LogInfo($"Log request: {formatted}");
                        }
                    }
                }
                catch { }
                finally
                {
                    requestList.Clear();
                }
            }

            if (_appSettingProvider.debugXml)
            {
                try
                {
                    foreach (string xmlDoc in logList)
                    {
                        if (_appSettingProvider.debug_file)
                            File.WriteAllText($"logResponse{iLog}.xml", xmlDoc);
                        else
                            Console.WriteLine(xmlDoc);
                    }
                }
                catch { }
                finally
                {
                    logList.Clear();
                }
            }

            return witsmlLog;
        }

        private bool CheckLogNames()
        {
            var query = new WitsmlQuery();

            query.addAttributeConstraint("log", "uidWell", _wellStatusInspector.ActiveWellId);
            query.addAttributeConstraint("log", "uidWellbore", _wellStatusInspector.ActiveWellboreId);

            query.excludeElement("logData");
            query.excludeElement("commonData");
            query.excludeElement("logCurveInfo");

            List<WitsmlLog> logs = _server.get<WitsmlLog>(query, new[] { _wellStatusInspector.ActiveWellboreId });

            if (logs.Any())
            {
                List<string> logNames = logs.Select(l => reduceLogId(l.getId(), l.getParentId())).ToList();

                string log10SecId = reduceLogId(_appSettingProvider.Log10secId, _wellStatusInspector.ActiveWellboreId);
                if (!string.IsNullOrEmpty(_appSettingProvider.Log10secId))
                {
                    _isLog10Present = logNames.Contains(_appSettingProvider.Log10secId);
                    if (!logNames.Contains(_appSettingProvider.Log10secId))
                    {
                        _logger.LogWarning($"Log '{_appSettingProvider.Log10secId}' not found on server.");
                    }
                }

                if (!logNames.Contains(reduceLogId(_appSettingProvider.LogId, _wellStatusInspector.ActiveWellboreId)))
                {
                    _logger.LogError($"Log '{_appSettingProvider.LogId}' not found on server.");
                    return false;
                }
                return true;
            }
            _logger.LogError($"No logs found on server.");

            return false;
        }

        private static string reduceLogId(string logId, string parentId)
        {
            if (string.IsNullOrEmpty(logId))
                return null;

            return (!string.IsNullOrEmpty(parentId) && logId.Contains(parentId) ? logId.Replace(parentId + "_", string.Empty) : logId);
        }

        private IEnumerable<WitsmlLog> GetWitsmlLogMany(DateTime startDateTimeIndex, DateTime endDateTimeIndex, string logId)
        {
            List<WitsmlLog> witsmlLogs = new List<WitsmlLog>();

            WitsmlLog log;
            startDateTimeIndex = startDateTimeIndex.AddSeconds(-300);
            DateTime newEndIndex = default(DateTime);
            do
            {
                _logger.LogInfo($"GET MANY: Getting log '{logId}' from {startDateTimeIndex} to {endDateTimeIndex}...");
                log = GetWitsmlLog(startDateTimeIndex, endDateTimeIndex, logId);
                if (log != null)
                {
                    newEndIndex = getEndIndex(log);
                    witsmlLogs.Add(log);
                    _logger.LogInfo($"GOT 10SEC: Got log '{logId}' data from {log.getStartIndex()} (endIndex: {newEndIndex}). RowCount: {log.getIndexCurve().getNValues()}.");
                    startDateTimeIndex = newEndIndex.AddSeconds(1);
                }
            } while (log != null && newEndIndex < endDateTimeIndex);

            return witsmlLogs;
        }

        private WitsmlDtoBase BuildRecord(JObject record, long unixTimestamp, DateTime recordTime)
        {
            return new WitsmlDtoBase
            {
                RecordTime = recordTime,
                Data = record,
                App = _appSettingProvider.App,
                Version = _appSettingProvider.Version,
                Asset_id = _appSettingProvider.AssetId,
                Company_id = _appSettingProvider.CompanyId,
                Timestamp = unixTimestamp,
                Provider = _appSettingProvider.Provider,
                Collection = _appSettingProvider.Collection
            };
        }

        private JObject GetWitsmlDataItems_old(int rowIndex, long unixTimestamp)
        {
            var json = new JObject { { "entry_at", unixTimestamp } };

            foreach (var curve in _model.WitsmlLog.getCurves())
            {
                var itemValue = curve.getValue(rowIndex);
                string name = curve.getName().ToLower();

                if (json[name] != null) continue;

                var unit = curve.getDataType();
                if (unit != typeof(DateTime))
                {
                    json.Add(name, new JValue(itemValue));
                }
                else
                {
                    if (name == "time")
                    {
                        continue;
                    }
                    var dateTime = itemValue as DateTime?;
                    if (dateTime.HasValue)
                    {
                        if (dateTime.Value.Kind != DateTimeKind.Utc)
                        {
                            dateTime = new DateTime(dateTime.Value.Ticks, DateTimeKind.Utc);
                        }
                        json.Add(name, new JValue(dateTime.Value));
                    }
                    else
                    {
                        json.Add(name, new JValue(itemValue));
                    }
                }
            }
            return json;
        }

        private JObject GetWitsmlDataItems(int rowIndex, long unixTimestamp)
        {
            var json = new JObject { { "entry_at", unixTimestamp } };

            AddPropertiesFromCurves(rowIndex, _model.WitsmlLog.getCurves(), json);

            if (_model.CombinedDictionary != null && _model.CombinedDictionary.TryGetValue(rowIndex, out IndexedDateTime indexedDateTime) && indexedDateTime != null)
            {
                WitsmlLog log = _model.Sec10Logs[indexedDateTime.LogNumber];
                int sec10LogIndex = indexedDateTime.Index;
                IEnumerable<WitsmlLogCurve> curves = log.getCurves().Where(c => _10SecCurves.Contains(c.getName().ToLower()));

                AddPropertiesFromCurves(sec10LogIndex, curves, json);
            }

            return json;
        }

        private static void AddPropertiesFromCurves(int rowIndex, IEnumerable<WitsmlLogCurve> curves, JObject json)
        {
            foreach (var curve in curves)
            {
                var itemValue = curve.getValue(rowIndex);
                string name = curve.getName().ToLower();

                if (json[name] != null) continue;

                var unit = curve.getDataType();
                if (unit != typeof(DateTime))
                {
                    json.Add(name, new JValue(itemValue));
                }
                else
                {
                    if (name == "time")
                    {
                        continue;
                    }
                    var dateTime = itemValue as DateTime?;
                    if (dateTime.HasValue)
                    {
                        if (dateTime.Value.Kind != DateTimeKind.Utc)
                        {
                            dateTime = new DateTime(dateTime.Value.Ticks, DateTimeKind.Utc);
                        }
                        json.Add(name, new JValue(dateTime.Value));
                    }
                    else
                    {
                        json.Add(name, new JValue(itemValue));
                    }
                }
            }
        }

        private int getTimestep(string logId)
        {
            return getTimestep(null, logId);
        }

        private int getTimestep(WitsmlLog log, string logId)
        {
            if (!_logTimestepsMap.ContainsKey(logId))
            {
                if (IsEmpty(log) || log.getIndexCurve().getNValues() < 2)
                {
                    _logger.LogInfo($"Trying to get spacing from log '{logId}' start...");

                    var query = new WitsmlQuery();
                    query.addElementConstraint("startDateTimeIndex", DateTime.Today.AddYears(-1));
                    query.addAttributeConstraint("log", "uidWell", _wellStatusInspector.ActiveWellId);
                    query.addAttributeConstraint("log", "uidWellbore", _wellStatusInspector.ActiveWellboreId);
                    query.addElementConstraint("dataRowCount", 2);

                    log = _server.getOne<WitsmlLog>(query, logId);
                }
                if (!IsEmpty(log))
                {
                    WitsmlLogCurve indexCurve = log.getIndexCurve();

                    int timestep = GetIndexCurveTotalSeconds(indexCurve);

                    if (timestep > 0)
                        _logTimestepsMap[logId] = timestep;

                    return timestep;
                }
            }

            return _logTimestepsMap.ContainsKey(logId) ? _logTimestepsMap[logId] : 10;
        }

        private int GetIndexCurveTotalSeconds(WitsmlLogCurve indexCurve)
        {
            return (int)((DateTime)indexCurve.getValue(1) - (DateTime)indexCurve.getValue(0)).TotalSeconds;
        }

        public DateTime GetNextStartIndex(DateTime lastIndex)
        {
            int logTimestep = getTimestep(_appSettingProvider.LogId);
            return lastIndex.AddSeconds(Math.Max(1, logTimestep));
        }

        public int GetWaitSeconds()
        {
            int timeStep = getTimestep(_isLog10Present ? _appSettingProvider.Log10secId : _appSettingProvider.LogId);
            //_logger.LogInfo($"timeStep : {timeStep}");

            return timeStep > 0 ? timeStep : 1;
        }
    }

    public class IndexedDateTime
    {
        public int LogNumber;
        public int Index;
        public DateTime? Timestamp;
        public long Ticks;
    }

    public class CombinedLog
    {
        public int Index;
        public IndexedDateTime Log10Sec;
    }
}
