﻿using Amazon;
using Amazon.Runtime;
using System;
using System.Threading.Tasks;
using Witsml.SourceApp.Interfaces;
using Amazon.S3.Model;
using Amazon.S3;
using System.IO;

namespace Witsml.SourceApp.DataRetrievers
{
    public class AmazonReader : IDataReader
    {
        private readonly ILogger _logger;
        private readonly IAppSettingProvider _appSettingProvider;
        private IAmazonS3 _S3Client;
        // private long _start;
        // private long _end;
        private Stream _objectStream;
        private StreamReader _streamReader;

        public AmazonReader(ILogger logger, IAppSettingProvider appSettingProvider)
        {
            _logger = logger;
            _appSettingProvider = appSettingProvider;
        }

        public bool Init()
        {
            return Task.Run(async () =>
            {
                var credentials = new BasicAWSCredentials(_appSettingProvider.AccessKey, _appSettingProvider.SecretKey);
                var regionEndpoint = RegionEndpoint.GetBySystemName(_appSettingProvider.RegionName);
                _S3Client = new AmazonS3Client(credentials, regionEndpoint);

                _objectStream = await _S3Client.GetObjectStreamAsync(_appSettingProvider.Bucket, _appSettingProvider.KeyName, null);
                _streamReader = new StreamReader(_objectStream);

                return true;
            }).GetAwaiter().GetResult();
        }

        public string ReadLine()
        {
            return _streamReader.ReadLine();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _streamReader?.Dispose();
                    _objectStream?.Dispose();
                    _S3Client?.Dispose();
                }

                disposedValue = true;
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
