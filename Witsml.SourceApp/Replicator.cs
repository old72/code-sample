﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Witsml.SourceApp.ApiRequests;
using Witsml.SourceApp.Factories;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Model;
using Witsml.SourceApp.Queue;
using Witsml.SourceApp.Settings;

namespace Witsml.SourceApp
{
    public class Replicator : IReplicator
    {
        private readonly ILogger _logger;
        private readonly IDataRetriever _dataRetriever;
        private readonly IAppSettingProvider _appSettingProvider;
        private readonly IDataSender<List<WitsmlDtoBase>> _dataSender;
        private readonly QueueManager<Datetimed<List<WitsmlDtoBase>>> _queueManager;
        private readonly StateManager _sm;
        private RunnerStatus _senderRunnerStatus;
        private RunnerStatus _retrieverRunnerStatus;
        private Task _sendDataTask;
        private Task _getDataAndSendToQueueTask;
        private bool _stopped;
        private DateTime _cutoffTime;
        private DateTime _endTime;
        private AutoResetEvent _autoEvent;
        private Timer _killTimer;
        private CancellationTokenSource _cts;
        private CancellationToken _getWitsmlToken;

        public Replicator(
            IAppSettingProvider appSettingProvider,
            ILogger logger,
            QueueManager<Datetimed<List<WitsmlDtoBase>>> queueManager,
            DataSenderFactory<List<WitsmlDtoBase>> dataSenderFactory,
            DataRetrieveFactory dataRetieveFactory,
            StateManager sm)
        {
            _appSettingProvider = appSettingProvider;
            _logger = logger;
            _queueManager = queueManager;
            _dataSender = dataSenderFactory.CreateDataSender();
            _dataRetriever = dataRetieveFactory.CreateDataRetiever();
            _sm = sm;
        }

        public void Start(AutoResetEvent autoEvent)
        {
            _autoEvent = autoEvent;

            DateTime startingTime = DateTime.Now;

            //_logger.LogInfo(startingTime.ToString("hh:mm:ss.FFF"));
            _killTimer = new Timer(StopMe, null, 1000 * _appSettingProvider.interval, Timeout.Infinite);

            _endTime = startingTime.AddSeconds(_appSettingProvider.interval);
            _cutoffTime = _endTime.AddMilliseconds(-50);

            _logger.LogInfo("Replicator start");

            _senderRunnerStatus = new RunnerStatus();
            _retrieverRunnerStatus = new RunnerStatus();

            _sm.ReadAndUpdateConfig();
            _appSettingProvider.SM = _sm;
            
            _appSettingProvider.ValidateSettings();

            _dataSender.SenderReady += DataSenderReady;
            _dataSender.SenderDisconnected += DataSenderDisconnected;
            _dataSender.Init();

            _dataRetriever.DataRetrieverReady += DataRetrieverDataRetrieverReady;
            _dataRetriever.DataRetrieveComplete += DataRetrieverDataRetrieveComplete;
            _dataRetriever.Connect();
        }

        private void StopMe(object state)
        {
            _logger.LogInfo("Kill timer elapsed...");
            Stop(true);
        }

        private void DataRetrieverDataRetrieveComplete(object sender, EventArgs e)
        {
            _dataSender.SenderDisconnected -= DataRetrieverDataRetrieveComplete;
            _logger.LogInfo("Data retrieve completed...");
            Task.Run(() => { Stop(); });
        }

        private void DataRetrieverDataRetrieverReady(object sender, EventArgs e)
        {
            _logger.LogInfo("DataRetriever ready...");
            _retrieverRunnerStatus.Cancelled = false;
            // retrieve data from witsml server and send data to queue process
            _getDataAndSendToQueueTask = GetWitsmlAndSendToQueue(_retrieverRunnerStatus);
        }

        private void DataSenderDisconnected(object sender, EventArgs e)
        {
            _logger.LogInfo("Data sender completed...");
            Task.Run(() => { Stop(true); });
        }

        private void DataSenderReady(object sender, EventArgs e)
        {
            _logger.LogInfo("DataSender ready...");
            _logger.LogInfo("Running replication process...");

            _cts = new CancellationTokenSource();
            _getWitsmlToken = _cts.Token;

            _senderRunnerStatus.Cancelled = false;

            // send data to kafka from queue process
            _sendDataTask = StartSendData(_senderRunnerStatus);
        }

        public void Stop(bool immediateStop = false)
        {
            if (_stopped)
            {
                return;
            }

            if (immediateStop)
            {
                _retrieverRunnerStatus.Cancelled = true;
                _senderRunnerStatus.Cancelled = true;

                _logger.LogInfo("Requesting cancellation inside data retriever...");
                _cts.Cancel();
            }

            try
            {
                _queueManager.MarkAsComplete();
                _logger.LogInfo("Waiting for running tasks...");

                if (_sendDataTask != null && _getDataAndSendToQueueTask != null)
                {
                    Task.WaitAll(_sendDataTask, _getDataAndSendToQueueTask);
                }
                else if (_sendDataTask == null)
                {
                    _getDataAndSendToQueueTask?.Wait();
                }
                else if (_getDataAndSendToQueueTask == null)
                {
                    _sendDataTask?.Wait();
                }
                _dataRetriever.Stop();

                setSchedulerComplete().Wait();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
            }
            _stopped = true;
            _logger.LogInfo("Replication process stopped...");
            _killTimer?.Dispose();
            _autoEvent.Set();
        }

        private async Task setSchedulerComplete()
        {
            var postRequest = new PostStatusRequest(_appSettingProvider.schedule, "completed");

            var response = await postRequest.ExecuteAsync();

            if (response.HttpResponseMessage.IsSuccessStatusCode)
            {
                _logger.LogInfo($"Scheduler update status: {response.Response.Status}");
            }
        }

        public async Task StartSendData(RunnerStatus runnerStatus)
        {
            await Task.Run(async () =>
            {
                while (!runnerStatus.Cancelled)
                {
                    await _queueManager.ProcessQueueItem(TrySendDataActionAsync, runnerStatus);
                }
                _dataSender.Dispose();
                _logger.LogInfo("Exiting from send data task...");
            });
        }

        public async Task GetWitsmlAndSendToQueue(RunnerStatus runnerStatus)
        {
            await Task.Run(async () =>
            {
                var startDateTimeIndex = GetStartUtcDateTime();
                while (!runnerStatus.Cancelled && !_dataRetriever.IsCompleted && DateTime.Now < _cutoffTime)
                {
                    try
                    {
                        if (_dataRetriever.GetWellData(startDateTimeIndex, _getWitsmlToken))
                        {
                            SendDataBatchesIntoQueue(runnerStatus, ref startDateTimeIndex);
                        }
                    }
                    catch (TaskCanceledException)
                    {
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e);
                    }
                    await Task.Delay(_dataRetriever.GetWaitSeconds() * 1000);
                }
                _logger.LogInfo("Exiting from replication task...");
            }, _getWitsmlToken);
        }

        private void SendDataBatchesIntoQueue(RunnerStatus runnerStatus, ref DateTime startDateTimeIndex)
        {
            var rowCount = _dataRetriever.GetRowCount();
            int startIndex = 0;
            while (startIndex < rowCount && !runnerStatus.Cancelled)
            {
                var witsmlData = _dataRetriever.GetWitsmlData(startIndex, _appSettingProvider.BatchSize);

                var startDatetime = witsmlData.First().RecordTime;
                var endDatetime = witsmlData.Last().RecordTime;
                var dtData = new Datetimed<List<WitsmlDtoBase>>(witsmlData)
                {
                    DateTime = endDatetime
                };

                _logger.LogInfo($"Adding to queue from {startDatetime} to {endDatetime}");
                if (_queueManager.AddToQueue(dtData))
                {
                    startDateTimeIndex = _dataRetriever.GetNextStartIndex(endDatetime);
                    startIndex += witsmlData.Count;
                }
                else if (!runnerStatus.Cancelled)
                {
                    _logger.LogInfo("Adding to queue failed (queue is full), waiting for the processing queue messages...");
                    Thread.Sleep(2000);
                }
            }
        }

        private async Task<bool> TrySendDataActionAsync(IEnumerable<Datetimed<List<WitsmlDtoBase>>> dtData, RunnerStatus runnerStatus)
        {
            _logger.LogInfo($"Sending using {_dataSender.GetType().Name} : {dtData.Sum(d => d.Data.Count)} records");
            if (await _dataSender.Send(dtData.Select(d => d.Data), runnerStatus))
            {
                SaveLastReplication(dtData.Max(d => d.DateTime));
                return true;
            }
            _logger.LogError("Sending data to socket failed.");
            return false;
        }

        private void SaveLastReplication(DateTime endDateTime)
        {
            _logger.LogInfo($"Saving last replication with endDateTime = {endDateTime}");
            _appSettingProvider.LastImportUtcDateTime = endDateTime;
        }

        private DateTime GetStartUtcDateTime()
        {
            if (_appSettingProvider.HardStartFromUtc != default(DateTime))
                return _appSettingProvider.HardStartFromUtc;

            if (_appSettingProvider.LastReadUtcDateTime != default(DateTime) && _appSettingProvider.LastReadUtcDateTime > _appSettingProvider.LastImportUtcDateTime )
                return _appSettingProvider.LastReadUtcDateTime;

            return _appSettingProvider.LastImportUtcDateTime == default(DateTime) ?
                _appSettingProvider.StartFromUtc : _appSettingProvider.LastImportUtcDateTime;
        }
    }
}
