﻿using Autofac;
using System;
using System.Collections.Generic;
using Witsml.SourceApp.Interfaces;
using Witsml.SourceApp.Logging;
using Witsml.SourceApp.Settings;
using Witsml.SourceApp.Queue;
using Witsml.SourceApp.Authorization;
using Witsml.SourceApp.Factories;
using Witsml.SourceApp.HistoricalImport;
using Witsml.SourceApp.WellStatusActivity;
using Witsml.SourceApp.Model;
using Witsml.SourceApp.DataRetrievers;

namespace Witsml.SourceApp.IoC
{
    public class Bootstrapper
    {
        private static IContainer _container;

        public static IContainer Init(string config = null)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<NLogLogger>().As<ILogger>().SingleInstance();
            builder.RegisterType<LambdaSettingsManager>().As<IAppSettingsManager>().WithParameter(new TypedParameter(typeof(string), config));
            builder.RegisterType<Replicator>().As<IReplicator>();

            builder.RegisterType<AppSettingProvider>().As<IAppSettingProvider>().SingleInstance();

            builder.RegisterType<QueueManager<Datetimed<List<WitsmlDtoBase>>>>().As<QueueManager<Datetimed<List<WitsmlDtoBase>>>>();

            builder.RegisterType<DataRetrieveFactory>().As<DataRetrieveFactory>();
            builder.RegisterType<WellStatusInspector>().As<IWellStatusInspector>();
            builder.RegisterType<FileDownloader>().As<IFileDownloader>();
            builder.RegisterType<CsvReader>().As<IFileReader>();
            builder.RegisterType<AmazonReader>().As<IDataReader>();

            builder.RegisterType<DataSenderFactory<List<WitsmlDtoBase>>>().As<DataSenderFactory<List<WitsmlDtoBase>>>();

            builder.RegisterType<Authorizer>().As<Authorizer>();
            builder.RegisterType<StateManager>().As<StateManager>();

            builder.RegisterType<RedisClient>().As<RedisClient>();

            _container = builder.Build();

           return _container;
       }

       public static IContainer GetContainer()
       {
           return _container;
       }
    }
}
