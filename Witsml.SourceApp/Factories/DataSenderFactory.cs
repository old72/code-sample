﻿using Autofac;
using Witsml.SourceApp.DataSenders;
using Witsml.SourceApp.Interfaces;

namespace Witsml.SourceApp.Factories
{
    public class DataSenderFactory<T>
    {
        private readonly IAppSettingProvider _appSettingProvider;
        private readonly ILogger _logger;

        public DataSenderFactory(IAppSettingProvider appSettingProvider, ILogger logger)
        {
            _appSettingProvider = appSettingProvider;
            _logger = logger;
        }

        public IDataSender<T> CreateDataSender()
        {
            if (_appSettingProvider.ExportDataToFile)
            {
                return new ExportToFileDataSender<T>(_appSettingProvider.FileNameToExport, _logger, _appSettingProvider);
            }
            if (_appSettingProvider.partition_number > 0)
            {
                return new MessageProducerDataSender<T>(_logger, _appSettingProvider);
            }
            return new KinesisDataProducer<T>(_logger, _appSettingProvider);
        }
    }
}
