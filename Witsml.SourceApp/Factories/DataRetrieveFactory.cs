﻿using Autofac;
using Witsml.SourceApp.DataRetrievers;
using Witsml.SourceApp.Interfaces;

namespace Witsml.SourceApp.Factories
{
    public class DataRetrieveFactory
    {
        private readonly IAppSettingProvider _appSettingProvider;
        private readonly IFileDownloader _fileDownloader;
        private readonly IFileReader _fileReader;
        private readonly ILogger _logger;
        private readonly IWellStatusInspector _wellStatusInspector;

        public DataRetrieveFactory(IFileDownloader fileDownloader, IAppSettingProvider appSettingProvider, IFileReader fileReader, ILogger logger, IWellStatusInspector wellStatusInspector)
        {
            _appSettingProvider = appSettingProvider;
            _fileDownloader = fileDownloader;
            _fileReader = fileReader;
            _logger = logger;
            _wellStatusInspector = wellStatusInspector;
        }

        public IDataRetriever CreateDataRetiever()
        {
            if (!string.IsNullOrEmpty(_appSettingProvider.Bucket))
            {
                return new HistoricalDataRetriever(_appSettingProvider, _fileReader, _logger);
            }
            return new WitsmlDataRetriever(_logger, _appSettingProvider, _wellStatusInspector);
        }
    }
}
