﻿using Newtonsoft.Json;

namespace Witsml.SourceApp.Model.ApiModel
{
    public class PostStatusResponse
    {
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}