﻿using Newtonsoft.Json;

namespace Witsml.SourceApp.Model.ApiModel
{
    public class AuthRequestBody
    {
        [JsonProperty("auth")]
        public Auth Auth { get; set; }
    }
}
