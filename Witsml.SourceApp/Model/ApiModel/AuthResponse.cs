﻿using Newtonsoft.Json;

namespace Witsml.SourceApp.Model.ApiModel
{
    public class AuthResponse
    {
        [JsonProperty("auth_key")]
        public string AuthKey { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("layer_identity")]
        public string LayerIdentity { get; set; }

        [JsonProperty("messaging_key")]
        public string MessagingKey { get; set; }
    }
}
