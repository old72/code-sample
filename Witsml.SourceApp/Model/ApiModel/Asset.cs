﻿using System;
using Newtonsoft.Json;

namespace Witsml.SourceApp.Model.ApiModel
{
    public class Asset
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("asset_type")]
        public string AssetType { get; set; }

        [JsonProperty("stats")]
        public string Stats { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("last_active_at")]
        public DateTime? LasActiveAt { get; set; }

        [JsonProperty("parent_asset_id")]
        public int? ParentAssetId { get; set; }
    }
}
