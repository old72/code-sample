﻿using Newtonsoft.Json;

namespace Witsml.SourceApp.Model.ApiModel
{
    public class AssetRequestBody
    {
        [JsonProperty("asset")]
        public Asset Asset { get; set; }
    }
}
