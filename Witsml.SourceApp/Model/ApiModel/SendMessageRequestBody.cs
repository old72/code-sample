﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared;

namespace Witsml.SourceApp.Model.ApiModel
{
    public class SendMessageRequestBody<T>
    {
        [JsonProperty("asset_id")]
        public int Asset_id { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }
    }
}
