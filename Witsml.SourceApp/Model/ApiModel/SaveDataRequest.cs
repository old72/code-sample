﻿namespace Witsml.SourceApp.Model.ApiModel
{
    public class StateResponse
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}