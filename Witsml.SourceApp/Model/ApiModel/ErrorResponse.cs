﻿using Newtonsoft.Json;

namespace Witsml.SourceApp.Model.ApiModel
{
    public class ErrorResponse
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("message")]
        public int Message { get; set; }
    }
}
