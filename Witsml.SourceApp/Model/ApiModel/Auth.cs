﻿using Newtonsoft.Json;

namespace Witsml.SourceApp.Model.ApiModel
{
   public class Auth
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
