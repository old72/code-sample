﻿using System.Net.Http;

namespace Witsml.SourceApp.Model.ApiModel
{
    public class ApiResponse<TResponse>
    {
        public TResponse Response { get; set; }

        public ErrorResponse ErrorResponse { get; set; }

        public HttpResponseMessage HttpResponseMessage { get; set; }


        public static ApiResponse<TResponse> ConstructApiResponse(HttpResponseMessage response, TResponse deserializedResponse, ErrorResponse errorResponse = null)
        {
            var apiResponse = new ApiResponse<TResponse>
            {
                HttpResponseMessage = response,
                ErrorResponse = errorResponse,
                Response = deserializedResponse
            };
            return apiResponse;
        }
    }
}
