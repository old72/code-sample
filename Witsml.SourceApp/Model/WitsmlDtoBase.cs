﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared;

namespace Witsml.SourceApp.Model
{
    public class WitsmlDtoBase : MessageHeader
    {
        [JsonIgnore]
        public DateTime RecordTime { get; set; }

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty("asset_id")]
        public int Asset_id { get; set; }

        [JsonProperty("company_id")]
        public int Company_id { get; set; }

        [JsonProperty("version")]
        public int Version { get; set; }

        [JsonProperty("data")]
        public JObject Data { get; set; }

        [JsonProperty("provider")]
        public string Provider { get; internal set; }

        [JsonProperty("collection")]
        public string Collection { get; internal set; }
    }
}
