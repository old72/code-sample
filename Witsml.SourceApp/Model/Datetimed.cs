﻿using System;

namespace Witsml.SourceApp.Model
{
    public class Datetimed<T>
    {
        public Datetimed(T witsmlData)
        {
            Data = witsmlData;
        }

        public T Data { get; set; }
        public DateTime DateTime { get; set; }
    }
}
