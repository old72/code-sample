﻿using nwitsml.Core;
using System.Collections.Generic;
using Witsml.SourceApp.DataRetrievers;

namespace Witsml.SourceApp.Model
{
    public class WitsmlWellModel
    {
        public WitsmlWell WitsmlWell { get; set; }
        public WitsmlWellbore WitsmlWellbore { get; set; }
        public WitsmlLog WitsmlLog { get; set; }
        public WitsmlLogCurve WitsmlIndexLogCurve { get; set; }
        public List<WitsmlLog> Sec10Logs { get; set; }
        public Dictionary<int, IndexedDateTime> CombinedDictionary { get; internal set; }
    }
}
