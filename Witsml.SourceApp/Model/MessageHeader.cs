﻿using Newtonsoft.Json;

namespace Witsml.SourceApp.Model
{
    public class MessageHeader
    {
        [JsonProperty("app")]
        public string App { get; set; }
    }
}
